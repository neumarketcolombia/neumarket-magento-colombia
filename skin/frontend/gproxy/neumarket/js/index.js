/* Upgrade for responsive */
jQuery(document).ready(function () {

    // Cart toggle
    jQuery('.cart-toggle').click(function() {
        jQuery(this).toggleClass('open');
        jQuery('#mini-cart-content').slideToggle();
    });
    //------

    // Modal Newsletter
    if(jQuery('.suscripcion-modal').length) {
        var modalNewsletterSubscriberFormDetail = new VarienForm('modal-newsletter-validate-detail');

        var bg = jQuery('<div class="bg-modal-suscription"></div>')
        jQuery('body').append(bg);
        jQuery(function () {

            jQuery('.bg-modal-suscription').css({opacity: 0, display: 'block'}).animate({opacity: 0.7}, 200);

            jQuery('.suscripcion-modal').css({opacity: 0, display: 'block'}).animate({opacity: 1}, 200, function () {
                jQuery('.bg-modal-suscription, .btn-close-modal, .btn-close-modal-top').click(function () {
                    jQuery('.suscripcion-modal, .bg-modal-suscription').animate({opacity: 0}, 200, function () {
                        jQuery('.suscripcion-modal, .bg-modal-suscription').css({display: 'none'});
                    });
                });
            });
        });
    }
    //------

    jQuery('.header').prepend('<span class="btnmain" id="main-button"><img src="/menu.png"></span>')
    jQuery('#home-A').prepend('<div class="imgLLanta"><img src="./llantas.png"></div>')
    jQuery('#home').prepend('<h1 id="home-F">¿Necesitas LLANTAS?</h1>')

    var ancho = jQuery(window).width()
    if (ancho <= 1010) {
        jQuery('.tabvehicle #form-wf-vehicles > div.buttons-set > button').prepend('<div>Buscar</div>')
        jQuery('#wheelsfinder-list-size > li:nth-child(4) > button').prepend('<div>Buscar</div>')
    }

    // logic tabs home
    var $boxVehicle = jQuery('.tabvehicle')
    var $boxSize = jQuery('.tabsize')

    jQuery('#home-A')
        .find('.tabcontent')
        .prepend('<div class="tabvehicle-tabs">' +
            '<div class="optVehicle">Búsqueda por <span>Vehículo</span></div>' +
            '<div class="optSize active">Búsqueda por <span>Dimensión</span></div>' +
            '</div > ')

    var $optVehicle = jQuery('.optVehicle')
    var $optSize = jQuery('.optSize')


    $optVehicle.click(function () {
        $optVehicle.addClass('active')
        $optSize.removeClass('active')
        $boxSize.hide(200)
        $boxVehicle.slideDown(500)
    })

    $optSize.click(function () {
        $optSize.addClass('active')
        $optVehicle.removeClass('active')
        $boxVehicle.hide(200)
        $boxSize.slideDown(500)
    })

    jQuery('.f-bottom').prepend('<ul class="footer-movil">' +
        '<h4> Comunicate con nosotros</h4 >' +
        '<div><strong>Bogotá: </strong><a href="tel:0317435595">(1)7435595</a></div>' +
        '<div><strong>Resto del país: </strong><a href="tel:01 8000 185 595">01 8000 185 595</a></div>' +
        '<div><strong>Whatsapp: </strong><a target="_blank" href="https://api.whatsapp.com/send?phone=573154806361">315 4806361</a></div>' +
        '<br><div class="wrapper-email-btn"><a class="email-btn" href="mailto:contacto@neumarket.com">Correo electrónico</a></div>' +
        '</ul > ')
    if (ancho <= 1010) {
        jQuery('.footer-container').prepend('<hr>')
        jQuery('.wrapper').append('<div class="line"></div>')
        jQuery('.ayuda').append('<img class="imgHelp" src="/help.png">')

    }


    jQuery('#main-button').click(function () {
        jQuery('#mainLinks').toggleClass('active')
        jQuery('.page').toggleClass('active')
        jQuery('.header-container').toggleClass('header-fixed')
    })
    var width = jQuery(window).width()
    if (width < 1010) {

        var $subMain = jQuery('#nav > li')
        $subMain.find('> a.level-top').click(function (event) {
            var $this = jQuery(this)
            event.preventDefault();
            $this.siblings('> a').attr('href', '')
            if ($this.siblings('ul').css('display') === 'block')
                $this.siblings('ul').slideToggle(500)
            else {
                $subMain.find('ul').slideUp()
                $this.siblings('ul').slideToggle(500)
            }
        })

        jQuery('.page').click(function () {
            var stateMenu = jQuery('#mainLinks').css('left')
            stateMenu = parseInt(stateMenu)
            if (stateMenu == 0) {
                jQuery('#mainLinks').toggleClass('active')
                jQuery('.header-container').toggleClass('header-fixed')
                jQuery('.page').toggleClass('active')
            }
        })
        jQuery('body > div.wrapper > div.header-container > div > h1 > a > img').attr('src','logo-m.png')
    }

    // Button cart header
    jQuery('.header .block-cart .cart-toggle .cart-toggle-wrap > div').prepend('<a href="/checkout/cart"><img class="car-m" src="/car.png"></a>')

    jQuery(".rslides").responsiveSlides({
        auto: true,             // Boolean: Animate automatically, true or false
        speed: 500,            // Integer: Speed of the transition, in milliseconds
        timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
        pager: false,           // Boolean: Show pager, true or false
        nav: false,             // Boolean: Show navigation, true or false
        random: false,          // Boolean: Randomize the order of the slides, true or false
        pause: false,           // Boolean: Pause on hover, true or false
        pauseControls: true,    // Boolean: Pause when hovering controls, true or false
        prevText: "Previous",   // String: Text for the "previous" button
        nextText: "Next",       // String: Text for the "next" button
        maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
        navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
        manualControls: "",     // Selector: Declare custom pager navigation
        namespace: "rslides",   // String: Change the default namespace used
        before: function(){},   // Function: Before callback
        after: function(){}     // Function: After callback
    });
    jQuery(".rslides2").responsiveSlides({
        auto: true,             // Boolean: Animate automatically, true or false
        speed: 500,            // Integer: Speed of the transition, in milliseconds
        timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
        pager: false,           // Boolean: Show pager, true or false
        nav: false,             // Boolean: Show navigation, true or false
        random: false,          // Boolean: Randomize the order of the slides, true or false
        pause: false,           // Boolean: Pause on hover, true or false
        pauseControls: true,    // Boolean: Pause when hovering controls, true or false
        prevText: "Previous",   // String: Text for the "previous" button
        nextText: "Next",       // String: Text for the "next" button
        maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
        navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
        manualControls: "",     // Selector: Declare custom pager navigation
        namespace: "rslides",   // String: Change the default namespace used
        before: function(){},   // Function: Before callback
        after: function(){}     // Function: After callback
    });



    // Grilla

    // Wrapper modal search
    var $sidebar = jQuery('.sidebar')
    $sidebar.wrap( "<div class='search-modal'></div>" );

    var $boxSearchVehicle = $sidebar.find('> div:nth-child(1)')
    var $boxSearchSize = $sidebar.find('> div.block-wheelsfinder-sizes')


    $sidebar.prepend('<section class="searchModal-tabs">' +
        '<div class="optSearchVehicle active">Búsqueda por <span>Vehículo</span></div>' +
        '<div class="optSearchSize">Búsqueda por <span>Dimensión</span></div>' +
        '</section > ')

    $sidebar.prepend('<section class="searchModal-close">cerrar [ x ]</section > ')

    $boxSearchSize.find('.actions')
        .append('<img  class="help_search" src="/help.png">')


    var $optSearchVehicle = jQuery('.optSearchVehicle')
    var $optSearchSize = jQuery('.optSearchSize')

    $optSearchVehicle.click(function () {
        $optSearchVehicle.addClass('active')
        $optSearchSize.removeClass('active')
        $boxSearchVehicle.slideDown(500)
        $boxSearchSize.hide(200)
    })

    $optSearchSize.click(function () {
        $optSearchSize.addClass('active')
        $optSearchVehicle.removeClass('active')
        $boxSearchSize.slideDown(500)
        $boxSearchVehicle.hide(200)
    })

    var ancho = jQuery(window).width()
    if (ancho <= 1010) {
        jQuery('#form-wf-vehicles-sidebar button').prepend('<div>Buscar</div>')
        jQuery('#form-wf-sizes-sidebar button').prepend('<div>Buscar</div>')
    }




    jQuery('.sorter')
        .append('<div class="filters">' +
            '<label>Filtros</label>' +
            '<div class="filters-arrow"> &#9660; </div>' +
            '</div > ')

    jQuery('.filters').click(function () {
        jQuery('.block-layered-nav').toggleClass('active')
        jQuery('.page').toggleClass('active')
        jQuery('.header-container').toggleClass('header-fixed')
    })

    jQuery('.block-layered-nav')
        .prepend('<div class="block-exit"><div class="exit-filter">  </div><div>Filtros:</div></div>')

    jQuery('.exit-filter').click(function () {
        jQuery('.block-layered-nav').toggleClass('active')
        jQuery('.page').toggleClass('active')
        jQuery('.header-container').toggleClass('header-fixed')
    })

    jQuery('.category-title')
        .append('<div class="btn-modal"><div> Cambiar </div><div class="bg-arrow" id="arrow-modalSearch"></div></div>')

    // Modal
    var $searchModal = jQuery('.search-modal')
    jQuery('.searchModal-close').click(function () {
        $searchModal.toggleClass('active')
        jQuery('#arrow-modalSearch').toggleClass('active')
    })
    jQuery('.btn-modal').click(function () {
        jQuery('#arrow-modalSearch').toggleClass('active')
        $searchModal.toggleClass('active')
    })

    // jQuery('.col-left.sidebar > div:nth-child(1) .block-content').css('display', 'block')

    // DETAIL
    if (ancho <= 1023) {
        var patchActual = window.location.pathname

        $title = jQuery('.product-shop .product-name')
        $iconos = jQuery('.especificaciones .iconos')
        $listServiteca = jQuery('.serviteca-list')
        $listServiteca.wrap( "<div class='serviteca-list-modal'></div>" );
        jQuery('.servitecas-left').append('<div class="serviteca-selector"><h4>Serviteca</h4><div class="bg-arrow" id="bg-arrow-servitecaList"></div><div class="serviteca-actualSelect"></div></div>')
        jQuery('.serviteca-list-modal').prepend('<section class="listServitecaModal-close">cerrar [ x ]</section > ')
        //Open modal servitecas
        jQuery('.serviteca-selector').click(function () {
            jQuery(this).find('.bg-arrow').toggleClass('active')
            jQuery('.serviteca-list-modal').toggleClass('active')
        })
        //Close modal servitecas
        jQuery('.listServitecaModal-close').click(function () {
            jQuery('.serviteca-list-modal').toggleClass('active')
            jQuery('#bg-arrow-servitecaList').toggleClass('active')
        })
        //Select serviteca from modal
        jQuery('.serviteca-item').click(function () {
            var content = jQuery(this).html()
            jQuery('.serviteca-actualSelect').html(content).addClass('active')
            jQuery('.serviteca-list-modal').toggleClass('active')
        })


        $contentTabOne = jQuery('#product_tabs_descripcion_contents')
        $contentTabTwo = jQuery('#product_tabs_additional_contents')
        $contentTabThree = jQuery('#product_tabs_envio_contents')

        $contentTabThree.css('display', 'block')
        $contentTabOne.before("<div class='tab-descripcion-content'><h4>Descripción</h4><div class='bg-arrow'></div></div>" );
        $contentTabTwo.before("<div class='tab-garantia-content'><h4>Garantía</h4><div class='bg-arrow'></div></div>" );
        $contentTabThree.before("<div class='tab-envio-content'><h4>Envío e instalación</h4><div class='bg-arrow active'></div></div>" );

        jQuery('.tab-descripcion-content').click(function () {
            $contentTabOne.slideToggle()
            $contentTabTwo.slideUp()
            $contentTabThree.slideUp()
            jQuery(this).find('.bg-arrow').toggleClass('active')
        })
        jQuery('.tab-garantia-content').click(function () {
            $contentTabTwo.slideToggle()
            $contentTabOne.slideUp()
            $contentTabThree.slideUp()
            jQuery(this).find('.bg-arrow').toggleClass('active')
        })
        jQuery('.tab-envio-content').click(function () {
            $contentTabThree.slideToggle()
            $contentTabOne.slideUp()
            $contentTabTwo.slideUp()
            jQuery(this).find('.bg-arrow').toggleClass('active')
        })

        jQuery('#product_tabs_envio_contents').prepend('<p>Neumarket cuenta con una Red Experta de Servitecas Asociadas en Colombia, en ellas podrá recoger o realizar la Instalación de sus llantas GRATIS: </p>')

        jQuery('.product-shop .product-name').clone().appendTo("#product_addtocart_form");

        jQuery('.product-tabs-content > br').remove()
        //Add Important Text
        jQuery('#buscador-servitecas').append('<p class="important"><span>Importante:</span> Recuerde que las llantas que se encuentran publicadas en Neumarket.com no están disponibles en las servitecas asociadas. Primero es necesario realizar el pedido de sus llantas, para enviarlas desde la bodega hacia la serviteca que usted escoja.</p>')
        // Add icons
        $iconos = jQuery('.iconos')
        jQuery('#buscador-servitecas').append($iconos)
        // Not available
        jQuery('.not-available').attr('src', '/Pocas_unidades.png')

        //CAR
        jQuery('.discount h2').html('¿TIENES UN CUPÓN DE DESCUENTOS?')
        jQuery('.discount').append('<div class="bg-arrow"></div>')
        jQuery('.col-2 .discount h2').click(function () {
            jQuery('.col2-set .col-2 .discount .discount-form').slideToggle()
            jQuery(this).siblings('.bg-arrow').toggleClass('active')
        })
    }

    var patchActual = window.location.pathname;
    if (patchActual == '/onestepcheckout' || patchActual == '/onestepcheckout/' || patchActual == '/checkout.html') {
        jQuery('.header-container, .footer-container').addClass('offAll')
        jQuery('body').addClass('offBgAll')
        jQuery('.wrapper').addClass('offBgAll').prepend('<div class="header-checkout">' +
            '<div><img src= "/logo_onestep.png" srcset= "/logo_onestep2X.png 2x" ></div>' +
            '<div class="header-checkout-right">' +
            '<div><a href="http://www.neumarket.com">Regresar a la tienda</a></div > ' +
            '<ul><li><img src="/check-cart.jpeg"><p>Pago <br>Contra Entrega</p></li><li><img src="/check-tel.jpeg"><p>(1)7435595 <br>018000185595</p></li>' +
            '<li><a href="https://trustsealinfo.websecurity.norton.com/splash?form_file=fdf/splash.fdf&dn=www.neumarket.com&lang=es" target="_blank"><img src="/check-norton.png" /></a></li></ul>' +
            '</div>' +
            '</div > ')
        jQuery('#onestepcheckout-form').append('<aside class="check-aside"><a href="https://trustsealinfo.websecurity.norton.com/splash?form_file=fdf/splash.fdf&dn=www.neumarket.com&lang=es" target="_blank">' +
            '<img src="/check-norton.png" alt="Norton Secured Certified" alt="Norton Secured Certified" /></a><p>Sitio protegido con Encriptación SSL y blindado contra el robo de información y clonación de tarjetas.</p></aside>' +
            '<p class="check-footer">Tiendas en linea de Colombia SAS | Neumarket | Todos los derechos reservados 2017</p>')
        var ancho = jQuery(window).width()
        if (ancho > 1023) {
            jQuery('#buscador-servitecas').wrap('<div class="modalCheckout-servitecaList"></div>');
            $modalCheckoutServitecasList = jQuery('.modalCheckout-servitecaList');
            $modalCheckoutServitecasList.append('<section class="searchModalCheckout-close">cerrar [ x ]</section > ');
            //Select serviteca from modal
            jQuery('.show_servitecas').append('<div class="checkout-serviteca-actualSelect" id="serviteca-selected"></div>');
            jQuery('.serviteca-item').click(function () {
                var content = jQuery(this).html()
                jQuery('.checkout-serviteca-actualSelect').html(content).addClass('active');
                $modalCheckoutServitecasList.toggleClass('active');
                get_save_billing_function('/onestepcheckout/ajax/save_billing', '/onestepcheckout/ajax/set_methods_separate', true, true)();
            });
            jQuery('.show_servitecas').click(function () {
                $modalCheckoutServitecasList.addClass('active')
            });
            $modalCheckoutServitecasList.find('.searchModalCheckout-close').click(function () {
                $modalCheckoutServitecasList.removeClass('active')
            });
            // $modalCheckoutServitecasList.find('.serviteca-item').click(function () {
            //     $modalCheckoutServitecasList.removeClass('active')
            // })
        } else {
            jQuery('#buscador-servitecas').addClass('servitecaListMovil')

        }
    }

    // CART
    if (ancho <= 766) {
        $columPrice = jQuery('.divTableRow > div:nth-child(4)')
        $columQty = jQuery('.divTableRow > div:nth-child(5)')
        $columRemove = jQuery('.divTableRow > div:nth-child(7)')
        $columSubtotal = jQuery('.divTableRow > div:nth-child(6)')
        $columSubtotal.prepend('<span class="labels">Subtotal: &nbsp;&nbsp;</span>')
        $columPrice.prepend('<span class="labels">Unidad: &nbsp;&nbsp;</span>')
        $columQty.prepend('<span class="labels">Cantidad: &nbsp;&nbsp;</span>')
    }

})