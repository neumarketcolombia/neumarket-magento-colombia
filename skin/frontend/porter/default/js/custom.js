jQuery.noConflict();
jQuery(document).ready(function(){

    jQuery(".rslides2").responsiveSlides({
        auto: true,             // Boolean: Animate automatically, true or false
        speed: 500,            // Integer: Speed of the transition, in milliseconds
        timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
        pager: false,           // Boolean: Show pager, true or false
        nav: false,             // Boolean: Show navigation, true or false
        random: false,          // Boolean: Randomize the order of the slides, true or false
        pause: false,           // Boolean: Pause on hover, true or false
        pauseControls: true,    // Boolean: Pause when hovering controls, true or false
        prevText: "Previous",   // String: Text for the "previous" button
        nextText: "Next",       // String: Text for the "next" button
        maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
        navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
        manualControls: "",     // Selector: Declare custom pager navigation
        namespace: "rslides",   // String: Change the default namespace used
        before: function(){},   // Function: Before callback
        after: function(){}     // Function: After callback
    });

    jQuery(".rslides3").responsiveSlides({
        auto: true,             // Boolean: Animate automatically, true or false
        speed: 500,            // Integer: Speed of the transition, in milliseconds
        timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
        pager: false,           // Boolean: Show pager, true or false
        nav: false,             // Boolean: Show navigation, true or false
        random: false,          // Boolean: Randomize the order of the slides, true or false
        pause: false,           // Boolean: Pause on hover, true or false
        pauseControls: true,    // Boolean: Pause when hovering controls, true or false
        prevText: "Previous",   // String: Text for the "previous" button
        nextText: "Next",       // String: Text for the "next" button
        maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
        navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
        manualControls: "",     // Selector: Declare custom pager navigation
        namespace: "rslides",   // String: Change the default namespace used
        before: function(){},   // Function: Before callback
        after: function(){}     // Function: After callback
    });


    jQuery('.filters').click(function () {
        jQuery('.block-layered-nav').toggleClass('active')
        jQuery('.page').toggleClass('active')
        jQuery('.header-container').toggleClass('header-fixed')
    });

    jQuery('.block-layered-nav').prepend('<div class="block-exit"><div class="exit-filter">  </div><div>Filtros:</div></div>');

    jQuery('.exit-filter').click(function () {
        jQuery('.block-layered-nav').toggleClass('active')
        jQuery('.page').toggleClass('active')
        jQuery('.header-container').toggleClass('header-fixed')
    });


    var ancho = jQuery(window).width();
    // DETAIL
    if (ancho <= 1023) {
        var patchActual = window.location.pathname

        $title = jQuery('.product-shop .product-name')
        $iconos = jQuery('.especificaciones .iconos')
        $listServiteca = jQuery('.serviteca-list')
        $listServiteca.wrap( "<div class='serviteca-list-modal'></div>" );
        jQuery('.servitecas-left').append('<div class="serviteca-selector"><h4>Serviteca</h4><div class="bg-arrow" id="bg-arrow-servitecaList"></div><div class="serviteca-actualSelect"></div></div>')
        jQuery('.serviteca-list-modal').prepend('<section class="listServitecaModal-close">cerrar [ x ]</section > ')
        //Open modal servitecas
        jQuery('.serviteca-selector').click(function () {
            jQuery(this).find('.bg-arrow').toggleClass('active')
            jQuery('.serviteca-list-modal').toggleClass('active')
        })
        //Close modal servitecas
        jQuery('.listServitecaModal-close').click(function () {
            jQuery('.serviteca-list-modal').toggleClass('active')
            jQuery('#bg-arrow-servitecaList').toggleClass('active')
        })
        //Select serviteca from modal
        jQuery('.serviteca-item').click(function () {
            var content = jQuery(this).html()
            jQuery('.serviteca-actualSelect').html(content).addClass('active')
            jQuery('.serviteca-list-modal').toggleClass('active')
        })


        $contentTabOne = jQuery('#product_tabs_descripcion_contents')
        $contentTabTwo = jQuery('#product_tabs_additional_contents')
        $contentTabThree = jQuery('#product_tabs_envio_contents')

        $contentTabThree.css('display', 'block')
        $contentTabOne.before("<div class='tab-descripcion-content'><h4>Descripción</h4><div class='bg-arrow'></div></div>" );
        $contentTabTwo.before("<div class='tab-garantia-content'><h4>Garantía</h4><div class='bg-arrow'></div></div>" );
        $contentTabThree.before("<div class='tab-envio-content'><h4>Envío e instalación</h4><div class='bg-arrow active'></div></div>" );

        jQuery('.tab-descripcion-content').click(function () {
            $contentTabOne.slideToggle()
            $contentTabTwo.slideUp()
            $contentTabThree.slideUp()
            jQuery(this).find('.bg-arrow').toggleClass('active')
        })
        jQuery('.tab-garantia-content').click(function () {
            $contentTabTwo.slideToggle()
            $contentTabOne.slideUp()
            $contentTabThree.slideUp()
            jQuery(this).find('.bg-arrow').toggleClass('active')
        })
        jQuery('.tab-envio-content').click(function () {
            $contentTabThree.slideToggle()
            $contentTabOne.slideUp()
            $contentTabTwo.slideUp()
            jQuery(this).find('.bg-arrow').toggleClass('active')
        })

        jQuery('#product_tabs_envio_contents').prepend('<p>Neumarket cuenta con una Red Experta de Servitecas Asociadas en Colombia, en ellas podrá recoger o realizar la Instalación de sus llantas GRATIS: </p>')

        jQuery('.product-shop .product-name').clone().appendTo("#product_addtocart_form");

        jQuery('.product-tabs-content > br').remove()
        //Add Important Text
        jQuery('#buscador-servitecas').append('<p class="important"><span>Importante:</span> Recuerde que las llantas que se encuentran publicadas en Neumarket.com no están disponibles en las servitecas asociadas. Primero es necesario realizar el pedido de sus llantas, para enviarlas desde la bodega hacia la serviteca que usted escoja.</p>')
        // Add icons
        $iconos = jQuery('.iconos')
        jQuery('#buscador-servitecas').append($iconos)
        // Not available
        jQuery('.not-available').attr('src', '/Pocas_unidades.png')

        //CAR
        jQuery('.discount h2').html('¿TIENES UN CUPÓN DE DESCUENTOS?')
        jQuery('.discount').append('<div class="bg-arrow"></div>')
        jQuery('.col-2 .discount h2').click(function () {
            jQuery('.col2-set .col-2 .discount .discount-form').slideToggle()
            jQuery(this).siblings('.bg-arrow').toggleClass('active')
        })
    }

    var patchActual = window.location.pathname;
    if (patchActual == '/onestepcheckout' || patchActual == '/onestepcheckout/' || patchActual == '/checkout.html') {

        if (ancho > 1023) {
            jQuery('#buscador-servitecas').wrap('<div class="modalCheckout-servitecaList"></div>');
            $modalCheckoutServitecasList = jQuery('.modalCheckout-servitecaList');
            $modalCheckoutServitecasList.append('<section class="searchModalCheckout-close">cerrar [ x ]</section > ');
            //Select serviteca from modal
            jQuery('.show_servitecas').append('<div class="checkout-serviteca-actualSelect" id="serviteca-selected"></div>');
            jQuery('.serviteca-item').click(function () {
                var content = jQuery(this).html()
                jQuery('.checkout-serviteca-actualSelect').html(content).addClass('active');
                $modalCheckoutServitecasList.toggleClass('active');
                get_save_billing_function('/onestepcheckout/ajax/save_billing', '/onestepcheckout/ajax/set_methods_separate', true, true)();
            });
            jQuery('.show_servitecas').click(function () {
                $modalCheckoutServitecasList.addClass('active')
            });
            $modalCheckoutServitecasList.find('.searchModalCheckout-close').click(function () {
                $modalCheckoutServitecasList.removeClass('active')
            });
            // $modalCheckoutServitecasList.find('.serviteca-item').click(function () {
            //     $modalCheckoutServitecasList.removeClass('active')
            // })
        } else {
            jQuery('#buscador-servitecas').addClass('servitecaListMovil')

        }
    }

    // CART
    if (ancho <= 766) {
        $columPrice = jQuery('.divTableRow > div:nth-child(4)')
        $columQty = jQuery('.divTableRow > div:nth-child(5)')
        $columRemove = jQuery('.divTableRow > div:nth-child(7)')
        $columSubtotal = jQuery('.divTableRow > div:nth-child(6)')
        $columSubtotal.prepend('<span class="labels">Subtotal: &nbsp;&nbsp;</span>')
        $columPrice.prepend('<span class="labels">Unidad: &nbsp;&nbsp;</span>')
        $columQty.prepend('<span class="labels">Cantidad: &nbsp;&nbsp;</span>')
    }
});