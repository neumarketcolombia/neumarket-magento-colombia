<?php

require_once dirname(__FILE__) . '/api/SupportedLanguages.php';
require_once dirname(__FILE__) . '/api/PayUKeyMapName.php';
require_once dirname(__FILE__) . '/api/PayUCommands.php';
require_once dirname(__FILE__) . '/api/PayUTransactionResponseCode.php';
require_once dirname(__FILE__) . '/api/PayUHttpRequestInfo.php';
require_once dirname(__FILE__) . '/api/PayUResponseCode.php';
require_once dirname(__FILE__) . '/api/PayuPaymentMethodType.php';
require_once dirname(__FILE__) . '/api/PaymentMethods.php';
require_once dirname(__FILE__) . '/api/PayUCountries.php';
require_once dirname(__FILE__) . '/exceptions/PayUErrorCodes.php';
require_once dirname(__FILE__) . '/exceptions/PayUException.php';
require_once dirname(__FILE__) . '/exceptions/ConnectionException.php';
require_once dirname(__FILE__) . '/api/PayUConfig.php';
require_once dirname(__FILE__) . '/api/RequestMethod.php';
require_once dirname(__FILE__) . '/util/SignatureUtil.php';
require_once dirname(__FILE__) . '/api/PaymentMethods.php';
require_once dirname(__FILE__) . '/api/TransactionType.php';
require_once dirname(__FILE__) . '/util/PayURequestObjectUtil.php';
require_once dirname(__FILE__) . '/util/PayUParameters.php';
require_once dirname(__FILE__) . '/util/CommonRequestUtil.php';
require_once dirname(__FILE__) . '/util/RequestPaymentsUtil.php';
require_once dirname(__FILE__) . '/util/UrlResolver.php';
require_once dirname(__FILE__) . '/util/PayUReportsRequestUtil.php';
require_once dirname(__FILE__) . '/util/PayUTokensRequestUtil.php';
require_once dirname(__FILE__) . '/util/PayUSubscriptionsRequestUtil.php';
require_once dirname(__FILE__) . '/util/PayUSubscriptionsUrlResolver.php';
require_once dirname(__FILE__) . '/util/HttpClientUtil.php';
require_once dirname(__FILE__) . '/util/PayUApiServiceUtil.php';

require_once dirname(__FILE__) . '/api/Environment.php';

require_once dirname(__FILE__) . '/PayUBankAccounts.php';
require_once dirname(__FILE__) . '/PayUPayments.php';
require_once dirname(__FILE__) . '/PayUReports.php';
require_once dirname(__FILE__) . '/PayUTokens.php';
require_once dirname(__FILE__) . '/PayUSubscriptions.php';
require_once dirname(__FILE__) . '/PayUCustomers.php';
require_once dirname(__FILE__) . '/PayUSubscriptionPlans.php';
require_once dirname(__FILE__) . '/PayUCreditCards.php';
require_once dirname(__FILE__) . '/PayURecurringBill.php';
require_once dirname(__FILE__) . '/PayURecurringBillItem.php';







/**
 *
 * Holds basic request information
 * 
 * @author PayU Latam
 * @since 1.0.0
 * @version 1.0.0, 20/10/2013
 *
 */
abstract class PayU {
	
	/**
	 * Api version
	 */
	const  API_VERSION = "4.0.1";

	/**
	 * Api name
	 */
	const  API_NAME = "PayU SDK";
	
	
	const API_CODE_NAME = "PAYU_SDK";

	/**
	 * The method invocation is for testing purposes
	 */
	public static $isTest = false;

	/**
	 * The merchant API key
	 */
	public static  $apiKey = null;

	/**
	 * The merchant API Login
	 */
	public static  $apiLogin = null;

	/**
	 * The merchant Id
	 */
	public static  $merchantId = null;

	/**
	 * The request language
	 */
	public static $language = SupportedLanguages::ES;
	

}


/** validates Environment before begin any operation */
	Environment::validate();

