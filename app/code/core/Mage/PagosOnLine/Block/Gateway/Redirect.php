<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_PagosOnLine
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * PagosOnLine Form Block
 *
 * @category    Mage
 * @package     Mage_PagosOnLine
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_PagosOnLine_Block_Gateway_Redirect extends Mage_Core_Block_Template
{
    /**
     * Return Pol payment url
     *
     * @return string
     */
    public function getRedirectUrl()
    {
        return Mage::registry('pagosonline_payment_gateway')->getPayRedirectUrl();
    }

    /**
     * Return pay params for current order
     *
     * @return array
     */
    public function getRedirectParams()
    { 
        return Mage::registry('pagosonline_payment_gateway')->getPayRedirectParams();
    }
    
    public function getDebugValue(){
    	return Mage::registry('pagosonline_payment_gateway')->getDebugValue();
    }
}
