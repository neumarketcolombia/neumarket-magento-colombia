<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_pagosonline
 * @copyright   Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * pagosonline Controller
 *
 * @category    Mage
 * @package     Mage_pagosonline
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_PagosOnLine_GatewayController extends Mage_Core_Controller_Front_Action
{
    /**
     * Get singleton with payment model PagosOnLine Gateway
     *
     * @return Mage_PagosOnLine_Model_Payment_Gateway
     */
    public function getPayment()
    {
        return Mage::getSingleton('pagosonline/payment_gateway');
    }

    /**
     * Get singleton with model checkout session
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * When a customer press "Place Order" button on Checkout/Review page
     * Redirect customer to Pagos On Line payment interface
     *
     */

    public function payAction()
    {
		$session = $this->getSession();

        $order = Mage::getModel('sales/order');
        $order->loadByIncrementId($session->getLastRealOrderId());

        if (Mage::getModel('customer/session')->isLoggedIn()) {
            $this->updateCustomer($session, $order);
        }
		//Mage::log('payAction', print_r($session));
        $quoteId = $session->getQuoteId();
        $lastRealOrderId = $session->getLastRealOrderId();
        if (is_null($quoteId) || is_null($lastRealOrderId)){
            $this->_redirect('checkout/cart/');
        } else {
			$session->setPolGatewayQuoteId($session->getQuoteId());
			$session->setPolGatewayLastRealOrderId($session->getLastRealOrderId());

			$payment = $this->getPayment();
			$payment->setOrder($order);
			$payment->processEventRedirect();

			Mage::register('pagosonline_payment_gateway', $payment);
			$this->loadLayout();
			$this->renderLayout();

			$quote = $session->getQuote();
            $quote->setIsActive(false);
            $quote->save();

            $session->setQuoteId(null);
            $session->setLastRealOrderId(null);
		}
    }

    /**
     * When a customer successfully returned from Pagos On Line Gateway site
     * Redirect customer to Checkout/Success page
     *
     */
    public function returnSuccessAction()
    {
        $session = $this->getSession();

        $order = Mage::getModel('sales/order');
        $order->loadByIncrementId($session->getPolGatewayLastRealOrderId());

        if ($order->isEmpty()) {
            return false;
        }

        $payment = $this->getPayment();
        $payment->setOrder($order);
        $payment->processEventReturnSuccess();
        $session->setPolMessage($this->getRequest()->getParam('mensaje'));
        $session->setPolPayMethod($this->getRequest()->getParam('medio_pago'));
        $session->setPolValor($this->getRequest()->getParam('valor'));
        $session->setPolRef($this->getRequest()->getParam('ref_pol'));

        $session->setQuoteId($session->getPolGatewayQuoteId(true));
        $session->getQuote()->setIsActive(false)->save();
        $session->setLastRealOrderId($session->getPolGatewayLastRealOrderId(true));
        //$this->_redirect('checkout/onepage/success');
        $this->_redirect('pagosonline/gateway/success');

    }

    /**
     * Update customer with Amasty Data (Custom attributes)
     *
     */
    private function updateCustomer($session, $order)
    {
        $dni = $session->getAmcustomerattr();
        $customer_id = $order->getCustomerId();
        $customer = Mage::getModel('customer/customer')->load($customer_id);

        // Si cliente NO tiene definidos los campos personalizados (Amasty)
        // y si se esta intentando registrar desde el checkout por primera vez:

        // Si el cliente es Persona
        if ($dni['client_type'] == '1386' && !empty($dni['nro_documento'])) {
            $customer->setData("nro_documento", $dni['nro_documento']);
            $customer->setData("tipo_documento", $dni['tipo_documento']);
            $customer->setData("client_type", $dni['client_type']);
            $customer->save();
        } // Si el cliente es Empresa
        else if ($dni['client_type'] == '1387' && !empty($dni['nit'])) {
            $customer->setData("nit", $dni['nit']);
            $customer->setData("razon_social", $dni['razon_social']);
            $customer->setData("client_type", $dni['client_type']);
            $customer->save();
        }
    }

    /**
     * Enter description here...
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    public function getOnepage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }
     public function successAction()
    {
        if (!$this->getOnepage()->getCheckout()->getLastSuccessQuoteId()) {
            $this->_redirect('checkout/cart');
            return;
        }

        $lastQuoteId = $this->getOnepage()->getCheckout()->getLastQuoteId();
        $lastOrderId = $this->getOnepage()->getCheckout()->getLastOrderId();

        if (!$lastQuoteId || !$lastOrderId) {
            $this->_redirect('checkout/cart');
            return;
        }

        Mage::getSingleton('checkout/session')->clear();
        $this->loadLayout();
        $this->_initLayoutMessages('checkout/session');
        //$this->_initLayoutMessages('pagosonline/session');
        //Mage::dispatchEvent('checkout_onepage_controller_success_action');
        Mage::dispatchEvent('pagosonline_gateway_controller_success_action');
        $this->renderLayout();

    }
    /**
      * Get singleton with Checkout by Amazon order transaction information
     *
     * @return Mage_AmazonPayments_Model_Payment_CBA
     */
    public function getGateway()
    {
        return Mage::getSingleton('pagosonline/payment_gateway');
    }

 	public function responseAction()
    {
        #$amazonOrderID = Mage::app()->getRequest()->getParam('amznPmtsOrderIds');
        #$referenceId = Mage::app()->getRequest()->getParam('amznPmtsOrderIds');

        $this->getGateway()->returnPol();

        $this->loadLayout();
        $this->_initLayoutMessages('pagosonline/session');
        $this->renderLayout();

    }

    /**
     * Customer canceled payment and successfully returned from Pagos On Line Gataway
     * Redirect customer to Shopping Cart page
     *
     */
    public function returnCancelAction()
    {
        $session = $this->getSession();
        $session->setQuoteId($session->getPolGatewayQuoteId(true));

        $order = Mage::getModel('sales/order');
        $order->loadByIncrementId($session->getPolGatewayLastRealOrderId());

        if ($order->isEmpty()) {
            return false;
        }

        $payment = $this->getPayment();
        $payment->setOrder($order);
        $payment->processEventReturnCancel();

        $this->_redirect('checkout/cart/');
    }

    /**
     * Pagos On Line Gateway service send notification
     *
     */
    public function notificationAction()
    {
    	 $archivo = 'PagosOnline_Notificaciones.txt';
		$fp = fopen($archivo, "a");
		$string = "\n param" .  print_r($_GET ,true) . print_r($this->getRequest()->getParams(),1) . print_r(Mage::getSingleton('admin/session')->getUser());
		$write = fputs($fp, $string);
		fclose($fp);

        $this->getPayment()->processNotification($this->getRequest()->getParams());
    }
}
