<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Model_Observer {

    protected $_protocol = null;
    protected $_enhancedEcommerceHelper = null;
    
    public function getMeasurementProtocol() {
        if(is_null($this->_protocol)) {
            $this->_protocol = Mage::getModel('googleuniversalanalytics/measurement_protocol');
        }
        return $this->_protocol;
    }
    
    public function getEcHelper() {
        if(is_null($this->_enhancedEcommerceHelper)) {
            $this->_enhancedEcommerceHelper = Mage::helper('googleuniversalanalytics/ecommerce');
        }
        return $this->_enhancedEcommerceHelper;
    }
    
    
    public function observeOrderCancelAfter($observer) {
        $order = $observer->getEvent()->getOrder();
        $this->_refundCanceledOrder($order);
        
    }
    
    
    public function observeControllerActionPredispatch($observer) {
        if ($observer->getEvent()->getControllerAction()->getFullActionName() == 'checkout_onepage_saveBilling') {
            $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveBilling');
        } else if ($observer->getEvent()->getControllerAction()->getFullActionName() == 'checkout_onepage_saveShipping') {
            $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveShipping');
        } else if ($observer->getEvent()->getControllerAction()->getFullActionName() == 'checkout_onepage_savePayment') {
            $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/savePayment');
        } else if ($observer->getEvent()->getControllerAction()->getFullActionName() == 'checkout_onepage_saveShippingMethod') {
            $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveShippingMethod');
        }
    }

    public function observeWishlistProductAddAfter($observer) {
        $items = $observer->getEvent()->getItems();
        if (Mage::getStoreConfig('googleuniversalanalytics/mp_events/wishlist_add_product')) {
            $productList = array();
            foreach ($items as $item) {
                $productList[] = $item->getProductId();
            }
            $this->getMeasurementProtocol()->sendEvent('wishlist', 'add_product', 'Products : ' . implode(', ', $productList) . ' added to wishlist', 1);
        }
    }

    public function observeWishlistShare($observer) {
        $wishlist = $observer->getEvent()->getWhishlist();
        if (Mage::getStoreConfig('googleuniversalanalytics/mp_events/wishlist_share')) {
            $this->getMeasurementProtocol()->sendEvent('wishlist', 'share', 'Customer #' . $wishlist->getCustomerId() . ' shared wishlist ', 1);
        }
    }

    public function observeSalesruleValidatorProcess($observer) {
        $rule = $observer->getEvent()->getRule();
        $item = $observer->getEvent()->getItem();
        $address = $observer->getEvent()->getAddress();
        $quote = $observer->getEvent()->getQuote();
        $qty = $observer->getEvent()->getQty();
        $result = $observer->getEvent()->getResult();
        if ($rule->getCouponCode() && Mage::getStoreConfig('googleuniversalanalytics/mp_events/checkout_add_coupon')) {
            $this->getMeasurementProtocol()->sendEvent('checkout', 'add_coupon', 'Add coupon code : ' . $rule->getCouponCode(), 1);
        }
    }

    public function observeNewsletterSubscriberSaveCommitAfter($observer) {
        $subscriber = $observer->getEvent()->getDataObject();
        $statusChange = $subscriber->getIsStatusChanged();
        if (Mage::getStoreConfig('googleuniversalanalytics/mp_events/newsletter_subscribe') && $subscriber->getSubscriberStatus() == "1" && $statusChange) {
            $this->getMeasurementProtocol()->sendEvent('newsletter', 'subscribe', 'New newsletter subscriber : ' . $subscriber->getId(), 1);
        }
        if (Mage::getStoreConfig('googleuniversalanalytics/mp_events/newsletter_unsubscribe') && $subscriber->getSubscriberStatus() == "0" && $statusChange) {
            $this->getMeasurementProtocol()->sendEvent('newsletter', 'unsubscribe', 'Newsletter unsubscribed : ' . $subscriber->getId(), 1);
        }
    }

    public function observeSalesQuoteRemoveItem($observer) {
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $product = $quoteItem->getProduct();
        if (Mage::app()->getStore()->isAdmin()) {
            return;
        }

        if (Mage::getStoreConfig('googleuniversalanalytics/mp_events/checkout_remove_product')) {
            $params = array();
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/mp_dimensions/product_name')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getName();
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/mp_dimensions/product_sku')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getSku();
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/mp_dimensions/product_id')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getProductId();
            }
            if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, 1)] = $this->getEcHelper()->getProductSkuValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_NAME, 1)] = $this->getEcHelper()->getProductNameValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_CATEGORY, 1)] = $this->getEcHelper()->getProductCategoryValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_BRAND, 1)] = $this->getEcHelper()->getProductBrandValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_VARIANT, 1)] = $this->getEcHelper()->getProductVariantValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_PRICE, 1)] = $quoteItem->getPriceInclTax()?$quoteItem->getPriceInclTax():$quoteItem->getPrice();
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'remove';
            }
            $this->getMeasurementProtocol()->sendEvent('checkout', 'remove_product', 'Remove product from cart #' . $quoteItem->getSku(), 1, false, false, null, $params);
        }
    }

    public function observeCheckoutCartProductAddAfter($observer) {
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $product = $observer->getEvent()->getProduct();
        if (Mage::app()->getStore()->isAdmin()) {
            return;
        }
        if (Mage::getStoreConfig('googleuniversalanalytics/mp_events/checkout_add_product')) {
            $params = array();
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/mp_dimensions/product_name')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $product->getName();
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/mp_dimensions/product_sku')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $product->getSku();
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/mp_dimensions/product_id')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $product->getId();
            }
            if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, 1)] = $this->getEcHelper()->getProductSkuValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_NAME, 1)] = $this->getEcHelper()->getProductNameValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_CATEGORY, 1)] = $this->getEcHelper()->getProductCategoryValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_BRAND, 1)] = $this->getEcHelper()->getProductBrandValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_VARIANT, 1)] = $this->getEcHelper()->getProductVariantValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_PRICE, 1)] = $quoteItem->getPriceInclTax()?$quoteItem->getPriceInclTax():$quoteItem->getPrice();
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'add';
            }
            $this->getMeasurementProtocol()->sendEvent('checkout', 'add_product', 'Add product to cart #' . $quoteItem->getSku(), 1, false, false, null, $params);
        }
    }

    public function observeCustomerLogin($observer) {
        $customer = $observer->getEvent()->getCustomer();
        if (Mage::getStoreConfig('googleuniversalanalytics/mp_events/customer_login')) {
            $this->getMeasurementProtocol()->sendEvent('customer', 'login', 'Customer login #' . $customer->getId(), 1);
        }
    }

    public function observeCustomerLogout($observer) {
        $customer = $observer->getEvent()->getCustomer();
        if (Mage::getStoreConfig('googleuniversalanalytics/mp_events/customer_logout')) {
            $this->getMeasurementProtocol()->sendEvent('customer', 'logout', 'Customer logout #' . $customer->getId(), 1);
        }
    }

    public function observeCustomerRegisterSuccess($observer) {
        $customer = $observer->getEvent()->getCustomer();
        $accountController = $observer->getEvent()->getAccountController();
        if (Mage::getStoreConfig('googleuniversalanalytics/mp_events/customer_register')) {
            $this->getMeasurementProtocol()->sendEvent('customer', 'registration', 'Customer registration #' . $customer->getId(), 1);
        }
    }

    public function observeCheckoutMultishippingControllerSuccessAction($observer) {
        $orderIds = $observer->getEvent()->getOrderIds();
        $this->_setGuaOnSuccessPageView($orderIds);
    }

    public function observeCheckoutOnepageControllerSuccessAction($observer) {
        $orderIds = $observer->getEvent()->getOrderIds();
        $this->_setGuaOnSuccessPageView($orderIds);
    }

    public function observeSalesOrderPlaceBefore($observer) {
        $order = $observer->getEvent()->getOrder();
        $this->_saveGuaClientId($order);
    }
    
    public function observeSalesOrderSaveAfter($observer) {
        $order = $observer->getEvent()->getOrder();
        if($order->getOrigData('status') == $order->getData('status')) {
            return;
        }
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/mp_transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CHANGE_ORDER_STATUS;
        $shouldSendTransaction = $shouldSendTransaction && !$order->getGuaSentFlag();
        $shouldSendTransaction = $shouldSendTransaction && $order->getData('status') == Mage::getStoreConfig('googleuniversalanalytics/mp_transactions/order_status_changed', $order->getStoreId());
        try {
            if ($shouldSendTransaction) {
                $this->_sendTransactionRequestFromOrder($order);
            }
            $this->getMeasurementProtocol()->sendEvent('sales', 'order_status', 'Order : ' . $order->getIncrementId() . ' has new status : ' . $order->getStatus(), 1, Mage::app()->getStore()->isAdmin(), $order->getGuaClientId());
        } catch (Exception $e) {
            Mage::log('Error :' . $e->getMessage(), null, 'gua.log');
        }
    }

    public function observeSalesOrderPlaceAfter($observer) {
       $order = $observer->getEvent()->getOrder();
       $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/mp_transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_PLACE_ORDER;
        
        try {
            $this->getMeasurementProtocol()->sendPageView('/gua/order/created', Mage::app()->getStore()->isAdmin(), $order->getGuaClientId());
            if ($shouldSendTransaction) {
                $this->_sendTransactionRequestFromOrder($order);
            }
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) { //If EE, we will send order as an event...
                $this->getMeasurementProtocol()->sendEvent('sales', 'order_placed', 'Order : ' . $order->getIncrementId() . ' has been placed', $order->getGrandTotal(), Mage::app()->getStore()->isAdmin(), $order->getGuaClientId());
            }
            Mage::getSingleton('checkout/session')->setGuaCheckoutStep(0);
        } catch (Exception $e) {
            Mage::log('Error :' . $e->getMessage(), null, 'gua.log');
        }
    }

    public function observeSalesOrderCreditmemoRefund($observer) {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order = $creditmemo->getOrder();
        try {
            $this->_sendRefundRequestFromCreditmemo($creditmemo, $order);
            $this->getMeasurementProtocol()->sendPageView('/gua/order/refunded', true, $order->getGuaClientId());
            if (!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->getMeasurementProtocol()->sendEvent('sales', 'order_refunded', 'Order : ' . $order->getIncrementId() . ' has been refunded', 1, true, $order->getGuaClientId());
            }
        } catch (Exception $e) {
            Mage::log('Error :' . $e->getMessage(), null, 'gua.log');
        }
    }
    
    public function observeSalesOrderInvoiceRegister($observer) {
        $invoice = $observer->getEvent()->getInvoice();
        $order = $observer->getEvent()->getOrder();
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/mp_transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CREATE_INVOICE;
        //Mage::log(Mage::getStoreConfig('googleuniversalanalytics/mp_transactions/transaction_event', $order->getStoreId()), null, 'gua.debug.log');
        try {
            if ($shouldSendTransaction) {
               $this->_sendTransactionRequestFromInvoice($invoice, $order);
            }
            $this->getMeasurementProtocol()->sendPageView('/gua/order/invoiced', true, $order->getGuaClientId());
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->getMeasurementProtocol()->sendEvent('sales', 'order_invoiced', 'Order : ' . $order->getIncrementId() . ' has been paid', 1, true, $order->getGuaClientId());
            }
        } catch (Exception $e) {
            Mage::log('Error :' . $e->getMessage(), null, 'gua.log');
        }
    }

    public function observeSalesOrderPaymentPay($observer) {
        $payment = $observer->getEvent()->getPayment();
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/mp_transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CAPTURE_INVOICE;
        try {
            if ($shouldSendTransaction) {
               $this->_sendTransactionRequestFromInvoice($invoice, $order);
            }
            $this->getMeasurementProtocol()->sendPageView('/gua/order/paid', true, $order->getGuaClientId());
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->getMeasurementProtocol()->sendEvent('sales', 'order_paid', 'Order : ' . $order->getIncrementId() . ' has been paid', 1, true, $order->getGuaClientId());
            }
        } catch (Exception $e) {
            Mage::log('Error :' . $e->getMessage(), null, 'gua.log');
        }
    }

    protected function _setGuaOnSuccessPageView($orderIds) {
        if (empty($orderIds) || !is_array($orderIds)) {
            return;
        }
        $block = Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('google_universal_analytics_pageview');
        if ($block) {
            $block->setOrderIds($orderIds);
        }
    }

    protected function _saveGuaClientId($order, $createIfNotFound=true) {
        $clientId = Mage::helper('googleuniversalanalytics')->getClientId($createIfNotFound);
        if ($clientId && !$order->getGuaClientId()) {
            $order->setGuaClientId($clientId)->save();
        }
    }

    protected function _getAdditionalParamsForOrder($order) {
        $params = array();
        
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        //ShoppingFlux Compatibility
        if ($order->getMarketplaceShoppingflux()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = $order->getMarketplaceShoppingflux();
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'Shopping Flux';
        }
        if (Mage::helper('wcooall')->isModuleInstalled('Ess_M2ePro')) {
            if(class_exists('Ess_M2ePro_Helper_Component_Amazon')) {
                try {
                    $collection = Mage::helper('M2ePro/Component_Amazon')->getCollection('Order');
                    $collection->getSelect()
                            ->joinLeft(
                                    array('so' => Mage::getSingleton('core/resource')->getTableName('sales/order')), '(so.entity_id = `main_table`.magento_order_id)', array('magento_order_num' => 'increment_id'))
                            ->where("`main_table`.`magento_order_id` = '" . $order->getId() . "'");
                    if ($collection->count() >= 1) {
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = 'Amazon';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'M2ePro';
                    }
                } catch (Exception $e) {
                    Mage::log($e->getMessage(), null, 'gua.m2epro.log');
                }
            }
            if(class_exists('Ess_M2ePro_Helper_Component_Ebay')) {
                try {
                    $collection = Mage::helper('M2ePro/Component_Ebay')->getCollection('Order');
                    $collection->getSelect()
                            ->joinLeft(
                                    array('so' => Mage::getSingleton('core/resource')->getTableName('sales/order')), '(so.entity_id = `main_table`.magento_order_id)', array('magento_order_num' => 'increment_id'))
                            ->where("`main_table`.`magento_order_id` = '" . $order->getId() . "'");
                    if ($collection->count() >= 1) {
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = 'eBay';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'M2ePro';
                    }
                } catch (Exception $e) {
                    Mage::log($e->getMessage(), null, 'gua.m2epro.log');
                }
            }
        }
        return $params;
    }
    
    public function observeSalesQuotePaymentSaveAfter($observer) {
        $payment = $observer->getEvent()->getPayment();
        if($payment->getData('method')) {
            $this->_sendCheckoutPaymentMethod($payment);
        }
    }
    
    public function observeSalesQuoteAddressSaveAfter($observer) {
        $address = $observer->getEvent()->getQuoteAddress();
        if($address->getAddressType() == 'shipping' && $address->getData('shipping_method')) {
            $this->_sendCheckoutShippingMethod($address);
        }
        if($address->getData('city')) {
            $this->_sendCheckoutAddress($address);
        }
    }
    
    protected function _sendCheckoutPaymentMethod($payment) {
        $storeId = $payment->getQuote()->getStoreId();
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_checkout', $storeId)) {
            return;
        }
        if (!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            return;
        }
        if(Mage::getSingleton('checkout/session')->getGuaCheckoutStep() != 3) {
            return;
        }
        $params = array();
        $items = $payment->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = 4;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $payment->getMethod();
        $this->getMeasurementProtocol()
                ->sendEvent('checkout', 'payment', $payment->getMethod(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep(4);
    }
    
    protected function _sendCheckoutAddress($quoteShippingAddress) {
        $storeId = $quoteShippingAddress->getQuote()->getStoreId();
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_checkout', $storeId)) {
            return;
        }
        if (!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            return;
        }
        $checkoutStep = $quoteShippingAddress->getAddressType()=='billing' ? 1 : 2;
        if($checkoutStep == 2 && Mage::getSingleton('checkout/session')->getGuaCheckoutStep() != 1) {
            return;
        } else if($checkoutStep == 1 && Mage::getSingleton('checkout/session')->getGuaCheckoutStep() >= 1) {
            return;
        }
       
        $params = array();
        $items = $quoteShippingAddress->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $checkoutStep;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $quoteShippingAddress->getPostcode() . ' ' .$quoteShippingAddress->getCity() . ' ' . $quoteShippingAddress->getCountryId();
        $this->getMeasurementProtocol()
                ->sendEvent('checkout', 'address', $quoteShippingAddress->getAddressType(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($checkoutStep);
    }
    
    protected function _sendCheckoutShippingMethod($quoteShippingAddress) {
        $storeId = $quoteShippingAddress->getQuote()->getStoreId();
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_checkout', $storeId)) {
            return;
        }
        if (!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            return;
        }
        if(Mage::getSingleton('checkout/session')->getGuaCheckoutStep() != 2) {
            return;
        }
        $params = array();
        $items = $quoteShippingAddress->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = 3;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $quoteShippingAddress->getShippingDescription()?$quoteShippingAddress->getShippingDescription():$quoteShippingAddress->getShippingMethod();
        $this->getMeasurementProtocol()
                ->sendEvent('checkout', 'shipping_method', $quoteShippingAddress->getShippingMethod(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep(3);
    }

    protected function _sendRefundRequestFromCreditmemo($creditmemo, $order) {
        $storeId = $order->getStoreId();
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_refund', $storeId)) {
            return;
        }
        if (!Mage::getStoreConfig('googleuniversalanalytics/mp_transactions/ecommerce', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        if (!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            return;
        }

        $params = array();
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        $items = $creditmemo->getAllItems();
        //$refundTotal = $creditmemo->getGrandTotal();
        
        /* Partial refunds are not working at the moment... Still in beta, so...
        $itemIncrement = 1;
        foreach($items as $item) {
            $hasParentItem = (bool) $item->getOrderItem()->getParentItemId();
            if(!$hasParentItem && $item->getQty() > 0) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, $itemIncrement)] = $item->getSku();
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_QUANTITY, $itemIncrement)] = $item->getQty();
                $itemIncrement++;
            }
        }
         */
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID] = $transactionId;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'refund';
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        $this->getMeasurementProtocol()
                ->sendEvent('transaction', 'creditmemo', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);
        
    }

    protected function _sendTransactionRequestFromInvoice($invoice, $order) {
        $storeId = $order->getStoreId();
        if (!$order->getGuaClientId()) {
            //Fix for Sage Pay extension
            $this->_saveGuaClientId($order, true);
        }
        if (!Mage::getStoreConfig('googleuniversalanalytics/mp_transactions/ecommerce', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        $revenue = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForInvoice($invoice);
        $shipping = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForInvoice($invoice);
        $tax = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForInvoice($invoice);
        $currency = $invoice->getOrderCurrencyCode();
        $coupon = $order->getCouponCode();
        $items = $invoice->getAllItems();
        $params = $this->_getAdditionalParamsForOrder($order);
        
        if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            $itemIncrement = 1;
            foreach($items as $item) {
                //$hasParentItem = (bool) $item->getOrderItem()->getParentItemId();
                //if(!$hasParentItem) {
                    $itemParams = $this->_getItemParams($item, $itemIncrement++);
                    $params = array_merge($params, $itemParams);
                //}
            }
            $transactionParams = $this->getMeasurementProtocol()->getTransactionParams($storeId, $transactionId, '', $revenue, $shipping, $tax, $currency, $coupon);
            $params = array_merge($params, $transactionParams);
            $this->getMeasurementProtocol()
                    ->sendEvent('transaction', 'invoice', $transactionId, $revenue, true, $guaClientId, $order->getStoreId(), $params);
        } else {
            $this->getMeasurementProtocol()
                    ->sendTransaction($transactionId, '', $revenue, $shipping, $tax, $currency, $guaClientId, true, $order->getStoreId(), $params);
            foreach($items as $item) {
                $this->getMeasurementProtocol()
                        ->sendItem($transactionId, $item->getName(), Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForInvoice($item), $item->getQty() ? $item->getQty() : $item->getQtyOrdered(), $item->getSku(), '', '', '', $currency, false, $guaClientId, 1, $order->getStoreId());
            }
        }
        $order->setGuaSentFlag(1)->save();
    }
    
    protected function _getItemParams($item, $itemIncrement) {
        $orderItem = $item->getOrderItem()?$item->getOrderItem():$item;
        $order = $orderItem->getOrder();
        $product = $orderItem->getProduct();
        $category = $this->getEcHelper()->getProductCategoryValue($product);
        $brand = $this->getEcHelper()->getProductBrandValue($product);
        $variant = $this->getEcHelper()->getProductVariantValue($product, $orderItem);
        $itemParams = $this->getMeasurementProtocol()->getItemParams($order?$order->getStoreId():$orderItem->getStoreId(), $itemIncrement, $item->getName(), Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($orderItem), $item->getQty() ? $item->getQty() : $item->getQtyOrdered(), $item->getSku(), $variant, $brand, $category, $product);
        return $itemParams;
    }

    protected function _sendTransactionRequestFromOrder($order) {
        $storeId = $order->getStoreId();
        if (!Mage::getStoreConfig('googleuniversalanalytics/mp_transactions/ecommerce', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        
        $revenue = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order);
        $shipping = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order);
        $tax = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order);
        $currency = $order->getOrderCurrencyCode();
        $items = $order->getAllVisibleItems();
        $coupon = $order->getCouponCode();
        $params = $this->_getAdditionalParamsForOrder($order);
        
        if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            $itemIncrement = 1;
            foreach($items as $item) {
                $itemParams = $this->_getItemParams($item, $itemIncrement++);
                $params = array_merge($params, $itemParams);
            }
            $transactionParams = $this->getMeasurementProtocol()->getTransactionParams($storeId, $transactionId, '', $revenue, $shipping, $tax, $currency, $coupon);
            $params = array_merge($params, $transactionParams);
            $this->getMeasurementProtocol()
                    ->sendEvent('transaction', 'order', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);
        } else {
            $this->getMeasurementProtocol()
                    ->sendTransaction($transactionId, '', $revenue, $shipping, $tax, $currency, $guaClientId, true, $storeId, $params);
            foreach($items as $item) {
                $this->getMeasurementProtocol()
                        ->sendItem($transactionId, $item->getName(), Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item), $item->getQty() ? $item->getQty() : $item->getQtyOrdered(), $item->getSku(), '', '', '', $currency, false, $guaClientId, 1, $order->getStoreId());
            }
        }
        $order->setGuaSentFlag(1)->save();
    }

    

    protected function _refundCanceledOrder($order) {
        $storeId = $order->getStoreId();
      
        if (!Mage::getStoreConfig('googleuniversalanalytics/mp_transactions/ecommerce', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_refund', $storeId)) {
            return;
        }
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/refund_canceled_order', $storeId)) {
            return;
        }
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/on_invoice', $storeId)) {
            return;
        }
      
        $params = array();
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        $items = $order->getAllItems();
        //$refundTotal = $order->getGrandTotal();
        
        
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID] = $transactionId;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'refund';
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        
        /* Partial refunds are not working at the moment... Still in beta, so...
        $itemIncrement = 1;
        foreach($items as $item) {
            $hasParentItem = (bool) $item->getParentItemId();
            if(!$hasParentItem && $item->getQtyCanceled() > 0) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, $itemIncrement)] = $item->getSku();
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_QUANTITY, $itemIncrement)] = $item->getQtyCanceled();
                $itemIncrement++;
            }
        }
        */
        
        $this->getMeasurementProtocol()
                ->sendEvent('sales', 'order_canceled', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);
       
    }


}
