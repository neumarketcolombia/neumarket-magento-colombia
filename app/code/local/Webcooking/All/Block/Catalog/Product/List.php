<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Block_Catalog_Product_List extends Mage_Catalog_Block_Product_List {

    protected $_limit = false;
    protected $_offset = false;
    protected $_toolbar = true;
    protected $_order = false;

    
    public function _construct() {
        parent::_construct();
        if(Mage::registry('category') && !$this->getCategoryId()) {
            $this->setCategoryId(Mage::registry('category')->getId());
        }
        if(Mage::registry('current_category') && !$this->getCategoryId()) {
            $this->setCategoryId(Mage::registry('current_category')->getId());
        }
    }
    
    
    /**
     *
     * @param <type> $attributeName
     * @param <type> $operator One or several for OR, comma separated
     * @param <type> $value One or several for OR, comma separated
     * @return <type>
     */
    public function addAttributeToFilter($attributeName, $operator, $value) {
        if (is_null($this->_productCollection))
            $this->_getProductCollection();
        $operators = explode(',', $operator);
        $values = explode(',', $value);
        if ($operator == 'IN') {
            $value = $values;
        } else if (count($operators) != count($values)) {
            return;
        }
        if (count($operators) == 1) {
            $this->_productCollection->addAttributeToFilter($attributeName, array($operator => $value));
        } else {
            $condition = array('or' => array());
            for ($i = 0; $i < count($operators); $i++) {
                $condition['or'][$i + 1] = array($operators[$i] => $values[$i]);
            }
            $this->_productCollection->addAttributeToFilter($attributeName, $condition);
        }
    }

    public function setLimit($limit, $offset = false) {
        if ($limit) {
            $this->_limit = $limit;
            $this->_offset = $offset;
            if (is_null($this->_productCollection))
                $this->_getProductCollection();
            if ($this->_productCollection) {
                if ($offset)
                    $this->_productCollection->getSelect()->limit($limit, $offset);
                else
                    $this->_productCollection->setPageSize($limit);//$this->_productCollection->getSelect()->limit($limit);
                $this->_toolbar = false;
                $this->reset();
                //Mage::log($this->_productCollection->getSelect().'', null, 'debuglist.log');
            }
        }
    }

    public function setOrder($field) {
        if ($field) {
            $this->_order = $field;
            if (is_null($this->_productCollection))
                $this->_getProductCollection();
            if ($this->_productCollection) {
                $this->_productCollection->setOrder($field);
                $this->reset();
            }
        }
    }

    public function productsInAtLeastXCategories($min) {
        $min = intval($min);
        if ($min <= 0)
            return;
        if (is_null($this->_productCollection))
            $this->_getProductCollection();
        if ($this->_productCollection) {
            $this->_productCollection->getSelect()->joinLeft(
                    array('cpi' => $this->_productCollection->getTable('catalog/category_product_index')), 'e.entity_id = cpi.product_id and cpi.store_id = ' . Mage::app()->getStore()->getId() . ' and cpi.category_id != ' . Mage::app()->getStore()->getRootCategoryId(), array('product_id', 'count_cat' => 'count(cpi.category_id)')
            );
            $this->_productCollection->getSelect()->joinRight(
                    array('cf' => $this->_productCollection->getTable('catalog/category_flat') . '_store_' . Mage::app()->getStore()->getId()), 'cf.entity_id = cpi.category_id and cf.is_active = 1', array('is_cat_active' => 'is_active')
            );

            $this->_productCollection->getSelect()->having('count(cpi.category_id) >= ?', $min);
            $this->_productCollection->getSelect()->group('cpi.product_id');
            //die ($this->_productCollection->getSelect());
            $this->reset();
        }
    }

    /**
     * Allow setMode('grid' or 'list') in layouts
     */
    public function getMode() {
        if ($this->getData('mode'))
            return $this->getData('mode');
        return $this->getToolbarBlock()->getCurrentMode();
    }

    public function reset() {
        if ($this->_productCollection && $this->_productCollection->isLoaded()) {
            $this->_productCollection->clear();
        }
    }

    protected function _beforeToHtml() {
        if ($this->_toolbar) {
            return parent::_beforeToHtml();
        }
        $toolbar = $this->getToolbarBlock();
        $this->_getProductCollection();
        if ($this->_productCollection) {

            if ($orders = $this->getAvailableOrders()) {
                $toolbar->setAvailableOrders($orders);
            }
            if ($sort = $this->getSortBy()) {
                $toolbar->setDefaultOrder($sort);
            }
            if ($dir = $this->getDefaultDirection()) {
                $toolbar->setDefaultDirection($dir);
            }

            if ($toolbar->getCurrentOrder()) {
                $this->_productCollection->setOrder($toolbar->getCurrentOrder(), $toolbar->getCurrentDirection());
            }
        }
        Mage::dispatchEvent('catalog_block_product_list_collection', array(
            'collection' => $this->_getProductCollection()
        ));

        return $this;
    }

    public function setCategoryId($categoryId) {
        if ($categoryId && is_numeric($categoryId) && $categoryId > 0) {
            $this->setData('category_id', $categoryId);
            $this->getLayer()->setData('category_id', $categoryId);
        }
    }
    
    
    
    public function prepareSortableFieldsByCategory($category) {
        if(!$category) {
            return $this;
        }
        return parent::prepareSortableFieldsByCategory($category);
    }

   
    
    
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();

            $this->_productCollection = $layer->getProductCollection();

            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

        }
        return $this->_productCollection;
    }
}