<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials controller
 *
 * @category   Nicolas
 * @package    Nicolas_Testimonials
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Brands_Adminhtml_BrandsController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('brands/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Manager Brand'), Mage::helper('adminhtml')->__('Manager Brand'))
			->getLayout()->getBlock('head')->setTitle($this->__('Manager Brand'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('brands/adminhtml_brands'));
        $this->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('brands/brand')->load($id);
		
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			
			if (!empty($data)) {
				$model->setData($data);
			}
			
			Mage::register('brands_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('brands/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Brand Manager'), Mage::helper('adminhtml')->__('Brand Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Brand News'), Mage::helper('adminhtml')->__('Brand News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('brands/adminhtml_brands_edit'))
				->_addLeft($this->getLayout()->createBlock('brands/adminhtml_brands_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('brands')->__('Brand does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			
			if($_FILES['image']['name'] != '') {
				try {
					 $uploader = new Varien_File_Uploader('image');
					 $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					 $uploader->setAllowRenameFiles(false);
					 $uploader->setFilesDispersion(false);
				 
					 $path = Mage::getBaseDir('media') . DS ;
							
					 $uploader->save($path, $_FILES['image']['name']);
					 
				} catch (Exception $e) {
					  
				}
				$data['image'] = $_FILES['image']['name'];
			}else {        
				if(isset($data['image']['delete']) && $data['image']['delete'] == 1){
					$data['image'] = '';
				}else{
					unset($data['image']);
				}
			}
			
			$model = Mage::getModel('brands/brand');
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				$new_brand = $model->save();
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('brands')->__('Brand was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('brands')->__('Unable to find brand'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				
				$model = Mage::getModel('brands/brands');
				
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Brands was successfully deleted'));
				
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $brandsIds = $this->getRequest()->getParam('brands');
        if(!is_array($brandsIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select brand(s)'));
        } else {
            try {
				
                foreach ($brandsIds as $brandsId) {
					$brands = Mage::getModel('brands/brands')->load($brandsId);
                    $brands->delete();
                }
				Mage::getSingleton('adminhtml/session')->addSuccess(
					Mage::helper('brands')->__(
						'Total of %d record(s) were successfully deleted ', count($brandsIds)
					)
				);
				
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    protected function _isAllowed()
    {
        return true;
    }

}