<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials
 *
 * @category   Np
 * @package    Np_Serviteca
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Brands_Model_System_Config_Source_Enabledisble
{
    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('Enable')),
            array('value' => 0, 'label'=>Mage::helper('adminhtml')->__('Disable')),
        );
    }

}
