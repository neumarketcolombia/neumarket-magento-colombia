<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials data helper
 *
 * @category   Nicolas
 * @package    Nicolas_Testimonials
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Brands_Helper_Data extends Mage_Core_Helper_Abstract
{
	protected function _getBrandModel()
	{
		return Mage::getModel('brands/brand');
	}
	public function getBrandImage($id = NULL)
	{
		if(!$id){
			$id = Mage::app()->getRequest()->getParam('id');
		}

		$model = $this->_getBrandModel()->load($id);
		$url = Mage::getBaseDir('media').DS;
		$image = $model->getImage();
		$image_142_70 = $url.'142-70'.DS.$image;
		$image_return = '';
		
		if(file_exists($image_142_70)){
			$image_return =  Mage::getBaseUrl('media').'142-70'.DS.$image;
		}
		
		if( file_exists($url.$image) && !file_exists($image_142_70) ){
			
			$image_obj = new Varien_Image($url.$image);
		
			$image_obj->constrainOnly(true);
			$image_obj->keepAspectRatio(true);
			$image_obj->backgroundColor(array(255, 255, 255));
			$image_obj->keeptransparency(false);
			$image_obj->quality(100);
			$image_obj->keepFrame(true);
			$image_obj->resize(142, 70);
			$image_obj->save($image_142_70);
			
			$image_return = Mage::getBaseUrl('media').'142-70'.DS.$image;
		}
		
		return $image_return;
	}
	
	public function getCurrentSelected()
	{
		$id = Mage::getSingleton('core/session')->getData('brandid');
		if(!$id){
			$id = 0;
		}
		return $id;
	}
}