<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Np_Brands_Block_Brand_List extends Mage_Catalog_Block_Product_List
{
	
	protected function _getSession()
	{
		return Mage::getSingleton('core/session');
	}
	
	protected function _getProductCollection()
    {

        /*$session = $this->_getSession();
		$brand_id = $session->getData('brandid');
		
		if($brand_id){
			
			$product_collection = Mage::getModel('catalog/product')
				->getCollection()
				->addAttributeToSelect('*')
				->addFieldToFilter('brands', $brand_id);
			
			$this->_productCollection = $product_collection;
			
		}else{
		
			if (is_null($this->_productCollection)) {
				
				$layer = $this->getLayer();
				$this->_productCollection = $layer->getProductCollection();
				
			}
		
		}*/

       return parent::_getProductCollection();
		
    }
	
	public function getSearchName()
	{
		$session = $this->_getSession();
		$brand_id = $session->getData('brandid');
		
		return Mage::getModel('brands/brand')->load($brand_id)->getName();
	}

}