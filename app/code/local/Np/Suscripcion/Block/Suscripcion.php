<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Np_Suscripcion_Block_Suscripcion extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
	
	public function isCustomerSubscriber()
	{
		return Mage::getModel('suscripcion/suscripcion')->isCustomerSubscriber();
	}
	
	public function showingCount()
	{
		$session = Mage::helper('suscripcion/data')->getCustomrSession();
		
		if(!$session->getData('showing_count')){
			
			$session->setData('showing_count', 1);
			
		}else{
			$count = $session->getData('showing_count');
			$session->setData('showing_count', ($count+1));
		}
		
		return $session->getData('showing_count');
		
	}
	
	public function getNewsletterBlock()
	{
		return Mage::app()->getLayout()->getBlockSingleton('newsletter/subscribe');
	}
	
	public function isRecentlySuscribed()
	{
		$session = Mage::helper('suscripcion/data')->getCustomrSession();
		return $session->getData('newsletter_suscription');
	}
}