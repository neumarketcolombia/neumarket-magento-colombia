<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials
 *
 * @category   Nicolas
 * @package    Nicolas_Wheelsfinder
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Suscripcion_Model_Suscripcion extends Mage_Core_Model_Abstract
{
	public function isCustomerSubscriber()
	{
		$loguedIn = Mage::helper('suscripcion/data')->getCustomrSession()->isLoggedIn();
		$status = null;
		
		if($loguedIn){
			
			$customer = Mage::helper('suscripcion/data')->getCustomrSession()->getCustomer();
			$email = $customer->getEmail();
			
			$subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($email);
			
			$status = $subscriber->isSubscribed();
			
		}
		
		return $status;
	}
}