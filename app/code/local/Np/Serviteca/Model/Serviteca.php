<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials
 *
 * @category   Nicolas
 * @package    Nicolas_Wheelsfinder
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Serviteca_Model_Serviteca extends Mage_Core_Model_Abstract
{
	const CACHE_TAG     = 'serviteca_serviteca';
	const MODEL_NAME	= 'serviteca';
	const MODULE_NAME	= 'serviteca';
    protected $_cacheTag= 'serviteca_serviteca';
	protected $_eventPrefix = 'serviteca_serviteca';
	protected $_model_id = NULL;
	
	protected function _construct()
    {
        $this->_init('serviteca/serviteca');
		$this->_model_id = $this->getId();
    }
	
	protected function _getModelId()
	{
		if(!$this->_model_id){
			$this->_model_id = $this->getId();
		}
		
		return $this->_model_id;
	}
	
	public function checkIdentifier($identifier)
    {
        return $this->_getResource()->checkIdentifier($identifier);
    }

}