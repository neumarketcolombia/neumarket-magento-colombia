<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Serviteca_Block_Adminhtml_Serviteca_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('serviteca_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('serviteca')->__('Serviteca Information'));
  }
  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('serviteca')->__('Serviteca Information'),
          'title'     => Mage::helper('serviteca')->__('Serviteca Information'),
          'content'   => $this->getLayout()->createBlock('serviteca/adminhtml_serviteca_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}