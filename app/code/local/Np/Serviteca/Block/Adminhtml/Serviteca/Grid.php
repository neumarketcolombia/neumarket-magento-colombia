<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Serviteca_Block_Adminhtml_Serviteca_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('servitecaGrid');
      $this->setDefaultSort('serviteca_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('serviteca/serviteca')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('serviteca_id', array(
          'header'    => Mage::helper('serviteca')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'serviteca_id',
      ));
	  
	  $this->addColumn('ciudad', array(
          'header'    => Mage::helper('serviteca')->__('Ciudad'),
          'align'     =>'left',
          'width'     => '200',
          'index'     => 'ciudad',
      ));
	  
      $this->addColumn('nombre', array(
          'header'    => Mage::helper('serviteca')->__('Nombre'),
          'align'     =>'left',
          'index'     => 'nombre',
      ));
	  
	  $this->addColumn('direccion', array(
          'header'    => Mage::helper('serviteca')->__('Direccion'),
          'align'     =>'left',
          'index'     => 'direccion',
      ));
	  
	  $this->addColumn('tel', array(
          'header'    => Mage::helper('serviteca')->__('Telefono'),
          'align'     =>'left',
          'width'     => '220',
          'index'     => 'tel',
      ));

      $this->addColumn('is_active', array(
          'header'    => Mage::helper('serviteca')->__('Status'),
          'align'     =>'center',
          'type'      =>'options',
          'index'     => 'is_active',
          'width'     => '140',
          'options'   => array( '1' => Mage::helper('serviteca')->__('Enabled'), '0' => Mage::helper('serviteca')->__('Disabled'), ),
      ));

      $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('serviteca')->__('Action'),
                'width'     => '80',
                'align'     =>'center',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('serviteca')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		//$this->addExportType('*/*/exportCsv', Mage::helper('wheelsfinder')->__('CSV'));
		//$this->addExportType('*/*/exportXml', Mage::helper('wheelsfinder')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('serviteca_id');
        $this->getMassactionBlock()->setFormFieldName('serviteca');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('serviteca')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('serviteca')->__('Are you sure?')
        ));
		
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}