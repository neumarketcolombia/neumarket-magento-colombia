<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
$installer = $this;
$installer->startSetup();

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('serviteca')};

CREATE TABLE IF NOT EXISTS {$this->getTable('serviteca')} (

  `serviteca_id` smallint(6) NOT NULL auto_increment,
  `is_active` tinyint(1) NOT NULL default '1',
  `ciudad` varchar(255) NOT NULL default '',
  `nombre` varchar(255) NOT NULL default '',
  `direccion` varchar(255) NOT NULL default '',
  `tel` varchar(255) NOT NULL default '',
  `creation_time` datetime default NULL,
  `update_time` datetime default NULL,
  
  PRIMARY KEY (`serviteca_id`)
  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup(); 