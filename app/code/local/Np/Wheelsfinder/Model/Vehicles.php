<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials
 *
 * @category   Nicolas
 * @package    Nicolas_Testimonials
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Wheelsfinder_Model_Vehicles extends Mage_Core_Model_Abstract
{
	const CACHE_TAG     = 'wheelsfinder_vehicles';
	const MODEL_NAME	= 'vehicles';
	const MODULE_NAME	= 'wheelsfinder';
    protected $_cacheTag= 'wheelsfinder_vehicles';
	protected $_eventPrefix = 'wheelsfinder_vehicles';
	protected $_model_id = NULL;
	
	protected function _construct()
    {
        $this->_init('wheelsfinder/vehicles');
		$this->_model_id = $this->getId();
    }
	
	protected function _getModelId()
	{
		if(!$this->_model_id){
			$this->_model_id = $this->getId();
		}
		
		return $this->_model_id;
	}
	
	protected function _clearDataSizesCreatedByVehicle($data)
	{
		if(is_array($data)){
			
			$valid_sizes = array();
			
			foreach($data as $size){
				if(!$size['is_delete'] && $size['ancho'] && $size['perfil'] && $size['rin']){
					unset($size['is_delete']);
					$valid_sizes[] = $size;
				}
			}
			
			return $valid_sizes;
			
		}else{
			return NULL;
		}
	}
	
	protected function _saveMultiplesSizes($data)
	{
		$sizes_saved = array();
		
		if($data){
		
			foreach($data as $size){

				$exist_size = $this->_getResource()->checkifSizeExist($size);
				
				if($exist_size){
					$sizes_saved[] = $exist_size['id'];
				}else{
				
					$new_size = Mage::getModel('wheelsfinder/sizes');
					$new_size->setData($size);
					
					if ($new_size->getCreatedTime == NULL || $new_size->getUpdateTime() == NULL) {
						$new_size->setCreatedTime(now())
							->setUpdateTime(now());
					} else {
						$new_size->setUpdateTime(now());
					}
					
					$new_size->save();
					
					$sizes_saved[] = $new_size->getId();
				
				}
	
			}
			
		}
		
		return $sizes_saved;
		
	}
	
	/*protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        return parent::_beforeSave($object);
    }*/

    protected function _afterSave()
    {
		$this->handleUrlRewrite();
		
        return parent::_afterSave();
    }
	
	protected function _beforeDelete()
    {
		$this->deleteUrlRewrites();
        return parent::_beforeDelete();
    }
	
	public function encodeUrlCaracteres($data_string)
	{
		$data_string = str_replace("/", "-", $data_string);
		$data_string = str_replace(" ", "_", $data_string);
		$data_string = str_replace(".", "", $data_string);
		$data_string = str_replace("&", "and", $data_string);
		
		$data_string = str_replace("á", "a", $data_string);
		$data_string = str_replace("é", "e", $data_string);
		$data_string = str_replace("í", "i", $data_string);
		$data_string = str_replace("ó", "o", $data_string);
		$data_string = str_replace("ú", "u", $data_string);
		$data_string = str_replace("ñ", "n", $data_string);
		
		$data_string = str_replace("(", "", $data_string);
		$data_string = str_replace(")", "", $data_string);
		$data_string = str_replace("+", "", $data_string);
		
		return $data_string;
		
	}
	
	protected function handleUrlRewrite()
	{
		$model_id = $this->_getModelId();
		$marca = $this->getMarca();
		$modelo = $this->getModelo();
		$linea = $this->getLinea();
		$request_identifier = $marca.'-'.$modelo.'-'.$linea;
		
		$request_identifier = $this->encodeUrlCaracteres($request_identifier);
		
		$id_path = self::MODEL_NAME.'/'.$model_id;
		$request_path = $request_identifier;
		$target_path = self::MODULE_NAME.'/'.self::MODEL_NAME.'/result/wf-marcas/'.$marca.'/wf-modelos/'.$modelo.'/wf-lineas/'.$linea;
		 
		$mainUrlRewrite = Mage::getModel('core/url_rewrite')->loadByIdPath($id_path);
		 
		if (!$mainUrlRewrite->isObjectNew()) {
			$urlRewriteCollection = Mage::getModel('core/url_rewrite')->getCollection()
				->addFilter('target_path', $mainUrlRewrite->getRequestPath())
				->addFieldToFilter('url_rewrite_id', array('neq' => $mainUrlRewrite->getUrlRewriteId()))
				->load();
		 
			foreach ($urlRewriteCollection as $urlRewrite) {
		 	
				if ($urlRewrite->getRequestPath() == $request_path) {
					$urlRewrite->delete();
				}else{
					$urlRewrite->setTargetPath($request_path)
						->setIsSystem(true)
						->setOptions('RP')
						->save();
				}
			}
		}
		
		$mainUrlRewrite->setIdPath($id_path)
			->setRequestPath($request_path)
			->setTargetPath($target_path)
			->setIsSystem(true)
			->save();
	}
	
	protected function deleteUrlRewrites()
	{
		$model_id = $this->_getModelId();
		$id_path = self::MODEL_NAME.'/'.$model_id;
		
		$mainUrlRewrite = Mage::getModel('core/url_rewrite')
			->loadByIdPath($id_path);
		
		if (!$mainUrlRewrite->isObjectNew()) {
			$urlRewriteCollection = Mage::getModel('core/url_rewrite')->getCollection()
				->addFilter('target_path', $mainUrlRewrite->getRequestPath())
				->addFieldToFilter('url_rewrite_id', array('neq' => $mainUrlRewrite->getUrlRewriteId()))
				->load();
			
			foreach ($urlRewriteCollection as $urlRewrite) {
				$urlRewrite->delete();
			}
		
		}
		
		$mainUrlRewrite->delete();
	}
	
	public function getUrl()
	{
		$target = self::MODULE_NAME.'/'.self::MODEL_NAME.'/result/';
		$marca = $this->getMarca();
		$modelo = $this->getModelo();
		$linea = $this->getLinea();
		
		return Mage::getUrl($target, array( 'wf-marcas' => $marca, 'wf-modelos' => $modelo, 'wf-lineas' => $linea ) );
	}
	
	public function reCreateUrlRewrite(){
		
		$model_id = $this->_getModelId();
		$id_path = self::MODEL_NAME.'/'.$model_id;
	
		if( $this->handleUrlRewrite()->getUrlRewriteId() ){
		
			$mainUrlRewrite = Mage::getModel('core/url_rewrite')
				->loadByIdPath($id_path);
			
			if ($mainUrlRewrite->getId()) {
				
				return Mage::getUrl($mainUrlRewrite->getRequestPath());
				
			} else {
				return false;
			}
			
		}else{
			return false;
		}
		
	}
	
	public function getUrlRewrite()
	{
		$model_id = $this->_getModelId();
		$id_path = self::MODEL_NAME.'/'.$model_id;
		
		$mainUrlRewrite = Mage::getModel('core/url_rewrite')
			->loadByIdPath($id_path);
			
			
		
		if ($mainUrlRewrite->getId()) {
			return Mage::getUrl($mainUrlRewrite->getRequestPath());
		} else {
			return $this->reCreateUrlRewrite();
		}
	}
	
	public function checkIdentifier($identifier)
    {
        return $this->_getResource()->checkIdentifier($identifier);
    }
	
	public function getVehiclesSizesByVechicleId($id){
		
		return $this->_getResource()->getVehiclesSizesByVechicleId($id);
		
	}
	
	public function setVehicleSize($vehicle_id, $sizes_id){
		return $this->_getResource()->setVehicleSize($vehicle_id, $sizes_id);
	}
	
	public function removeVehicleSizes($vehicle_id){
		return $this->_getResource()->removeVehicleSizes($vehicle_id);
	}
	
	public function saveSizesCreatedByVehicle($data)
	{
		$data_clear = $this->_clearDataSizesCreatedByVehicle($data);
		$ids_saved = $this->_saveMultiplesSizes($data_clear);
		
		return $ids_saved;
	}
	
	public function getVehicleIdByFields($marca, $modelo, $linea)
	{
		return $this->_getResource()->getVehicleIdByFields($marca, $modelo, $linea);
	}
	
	public function getModelosByMarca($marca)
	{
		$collection = $this->getCollection()
			->addFilter('marca', $marca)
			->setOrder('modelo', 'asc');
			
		$modelos = array();
		
		foreach($collection as $vehicle){
			
			if( !in_array($vehicle->getModelo(), $modelos) ){
				$modelos[] = $vehicle->getModelo();
			}
			
		}
		return $modelos;
	}
	
	public function getLineasByMarcaAndModelo($marca, $modelo)
	{
		
		$collection = $this->getCollection()
			->addFilter('marca', $marca)
			->addFilter('modelo', $modelo)
			->setOrder('linea', 'asc');
			
			
			
		$lineas = array();
		
		foreach($collection as $vehicle){
			
			if( !in_array($vehicle->getLinea(), $lineas) ){
				$lineas[] = $vehicle->getLinea();
			}
			
		}
		
		return $lineas;
	}
}