<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Cms
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Cms page mysql resource
 *
 * @category    Mage
 * @package     Mage_Cms
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Np_Wheelsfinder_Model_Resource_Vehicles extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * wheelsfinder/vehicles_sizes
     *
     * @var null
     */
    protected $_vehicles_sizes_table  = null;
	
	/**
     * wheelsfinder/sizes
     *
     * @var null
     */
    protected $_sizes_table  = null;

    /**
     * Initialize resource model
     *
     */
	 
	 protected $_vehicles_table  = null;

    /**
     * Initialize resource model
     *
     */
	 
    protected function _construct()
    {
        $this->_init('wheelsfinder/vehicles', 'vehicles_id');
    }
	
	protected function _getVehicleSizesTable()
	{
		if(!$this->_vehicles_sizes_table){
			$this->_vehicles_sizes_table = $this->getTable('wheelsfinder/vehicles_sizes');
		}
		
		return $this->_vehicles_sizes_table;
	}
	
	protected function _getSizesTable()
	{
		if(!$this->_sizes_table){
			$this->_sizes_table = $this->getTable('wheelsfinder/sizes');
		}
		
		return $this->_sizes_table;
	}
	
	protected function _getVehiclesTable()
	{
		if(!$this->_vehicles_table){
			$this->_vehicles_table = $this->getTable('wheelsfinder/vehicles');
		}
		
		return $this->_vehicles_table;
	}
	
	public function checkIdentifier($identifier)
    {
        return $this->_getResource()->checkIdentifier($identifier);
    }
	
	public function getVehiclesSizesByVechicleId($id = NULL)
	{
		if(!$id){
			return;
		}
		
		$table  = $this->_getVehicleSizesTable();
        $adapter = $this->_getReadAdapter();
		$sizes_ids = array();

        $select  = $adapter->select()
            ->from($table, 'size_id')
            ->where('vehicle_id = ?',(int)$id);
		
        $result = $adapter->fetchAll($select);
		
		if(count($result)){
			foreach($result as $id_array){
				$sizes_ids[] = $id_array['size_id'];		
			}
		}
		return $sizes_ids;
	}
	
	public function checkVehicleSizes($vehicle_id, $sizes_id)
	{
		$table  = $this->_getVehicleSizesTable();
		$adapter = $this->_getReadAdapter();
		
		$select  = $adapter->select()
            ->from($table, 'size_id')
            ->where('vehicle_id = ?',(int)$vehicle_id);
			
		$sizes_ids_saved = $adapter->fetchAll($select);
		$sizes_ids_invalid = array();
		
		foreach($sizes_ids_saved as $id){
			if(!in_array($id['size_id'], $sizes_id)){
				$sizes_ids_invalid[] = $id;
			}
		}
		
		return array('saved' => $sizes_ids_saved, 'errors' => $sizes_ids_invalid);
	}
	
	public function addVehicleSizes($vehicle_id, $sizes_id)
	{
		$table  = $this->_getVehicleSizesTable();
		$adapter = $this->_getWriteAdapter();	
		$data = array();
		
		foreach ($sizes_id as $id) {
			
			$row = array(
				'vehicle_id'  => (int) $vehicle_id,
				'size_id' => (int) $id
			);
			
			if(!in_array($row, $data)){
				$data[] = $row;
			}
		}
		
		return $adapter->insertMultiple($table, $data);
		
	}
	
	public function removeVehicleSizes($vehicle_id)
	{
		$table  = $this->_getVehicleSizesTable();
		$adapter    = $this->_getWriteAdapter();
        $condition  = array(
			'vehicle_id = ?' => $vehicle_id
		);
		
		return $adapter->delete($table, $condition);
		
	}
	
	public function setVehicleSize($vehicle_id = NULL, $size_ids)
	{
		if(!$vehicle_id){
			return;
		}
		
		$del = $this->removeVehicleSizes($vehicle_id);
		$insert = $this->addVehicleSizes($vehicle_id, $size_ids);
		$check = $this->checkVehicleSizes($vehicle_id, $size_ids);
		
		return $check;
		
	}
	
	public function checkifSizeExist($size)
	{
		$table  = $this->_getSizesTable();
		$adapter = $this->_getReadAdapter();
		
		$select  = $adapter->select()
            ->from($table, 'sizes_id')
            ->where('ancho = ?',(int)$size['ancho'])
			->where('perfil = ?',(int)$size['perfil'])
			->where('rin = ?',(int)$size['rin']);
		
		$result = $adapter->fetchCol($select);
		
		if(count($result)){
			return array('id'=>$result[0]);
		}else{
			return NULL;
		}
		
	}
	
	public function getVehicleIdByFields($marca, $modelo = NULL, $linea = NULL)
	{
		$table  = $this->_getVehiclesTable();
		$adapter = $this->_getReadAdapter();
		
		//print_r($marca);
		//print_r($modelo);
		//print_r($linea);
		//exit();
		
		$select  = $adapter->select()
			->from($table, 'vehicles_id');
		
		if($marca){
			$select->where('marca = ?', $marca);
		}
		
		if($modelo){
			$select->where('modelo = ?', $modelo);
		}
		
		if($linea){
			$select->where('linea = ?', $linea);
		}
		
		$result = $adapter->fetchRow($select);
		
		//var_dump(is_array($result)); echo count($result); exit();
		
		//Array ( [vehicles_id] => 26 ) Audi 2010 A5
		//Array ( [vehicles_id] => 1 ) BMW 2012 135i
						
		if($result && is_array($result)){
			return $result['vehicles_id'];
		}else{
			return NULL;
		}
	}
}
