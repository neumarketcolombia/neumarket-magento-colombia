<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Cms
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Cms page mysql resource
 *
 * @category    Mage
 * @package     Mage_Cms
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Np_Wheelsfinder_Model_Resource_Sizes extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * wheelsfinder/sizes
     *
     * @var null
     */
    protected $_sizes_table  = null;
	
	 /**
     * wheelsfinder/vehicles_sizes
     *
     * @var null
     */
    protected $_vehicles_sizes_table  = null;

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('wheelsfinder/sizes', 'sizes_id');
    }
	
	protected function _getVehicleSizesTable()
	{
		if(!$this->_vehicles_sizes_table){
			$this->_vehicles_sizes_table = $this->getTable('wheelsfinder/vehicles_sizes');
		}
		
		return $this->_vehicles_sizes_table;
	}
	
	protected function _getSizesTable()
	{
		if(!$this->_sizes_table){
			$this->_sizes_table = $this->getTable('wheelsfinder/sizes');
		}
		
		return $this->_sizes_table;
	}
	
	public function checkIdentifier($identifier)
    {
        return $this->_getResource()->checkIdentifier($identifier);
    }
	
	public function _getVehiclesSizesBySizeId($id)
	{
		$table  = $this->_getVehicleSizesTable();
		$adapter = $this->_getReadAdapter();
		
		$select  = $adapter->select()
            ->from($table)
            ->where('size_id = ?',(int)$id);
			
		return $adapter->fetchAll($select);
	}
	
	public function unsetVehiclesSizes($id)
	{
		$rows = $this->_getVehiclesSizesBySizeId($id);
		$vehicles_ids = array();
		$return_data;
		
		if(count($rows)){
			foreach($rows as $row){
				$vehicles_ids[]	= $row['vehicle_id'];
			}
			
			$table  = $this->_getVehicleSizesTable();
			$adapter    = $this->_getWriteAdapter();
			$condition  = array(
				'size_id = ?' => $id
			);
			
			$result = $adapter->delete($table, $condition);
			
			$return_data = array('vehicles_ids_updates'=>$vehicles_ids, 'size_deleted'=>$result);
		}else{
			$return_data = NULL;
		}
		
		return $return_data;
	}
	
	public function getSizeIdByFields($ancho, $perfil = NULL, $rin = NULL)
	{
		$table  = $this->_getSizesTable();
		$adapter = $this->_getReadAdapter();
		
		$select  = $adapter->select()
			->from($table, 'sizes_id');
		
		if($ancho){
			$select->where('ancho = ?', $ancho);
		}
		
		if($perfil){
			$select->where('perfil = ?', $perfil);
		}
		
		if($rin){
			$select->where('rin = ?', $rin);
		}
		
		$result = $adapter->fetchRow($select);
						
		if(count($result)){
			return $result['sizes_id'];
		}else{
			return NULL;
		}
	}
}
