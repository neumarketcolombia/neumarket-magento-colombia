<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials
 *
 * @category   Nicolas
 * @package    Nicolas_Wheelsfinder
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Wheelsfinder_Model_Sizes extends Mage_Core_Model_Abstract
{
	const CACHE_TAG     = 'wheelsfinder_sizes';
	const MODEL_NAME	= 'sizes';
	const MODULE_NAME	= 'wheelsfinder';
    protected $_cacheTag= 'wheelsfinder_sizes';
	protected $_eventPrefix = 'wheelsfinder_sizes';
	protected $_model_id = NULL;
	
	protected function _construct()
    {
        $this->_init('wheelsfinder/sizes');
		$this->_model_id = $this->getId();
    }
	
	protected function _getModelId()
	{
		if(!$this->_model_id){
			$this->_model_id = $this->getId();
		}
		
		return $this->_model_id;
	}
	
	protected function _afterSave()
    {
		$this->handleUrlRewrite();
        return parent::_afterSave();
    }
	
	protected function _beforeDelete()
    {
		$this->deleteUrlRewrites();
        return parent::_beforeDelete();
    }
	
	protected function handleUrlRewrite()
	{
		$model_id = $this->_getModelId();
		$ancho = $this->getAncho();
		$perfil = $this->getPerfil();
		$rin = $this->getRin();
		$request_identifier = $ancho.'-'.$perfil.'-'.$rin;
		
		$id_path = self::MODEL_NAME.'/'.$model_id;
		$request_path = (string)'llantas-'.$request_identifier;
		$target_path = self::MODULE_NAME.'/'.self::MODEL_NAME.'/result/wf-anchos/'.$ancho.'/wf-perfiles/'.$perfil.'/wf-rins/'.$rin;
		 
		$mainUrlRewrite = Mage::getModel('core/url_rewrite')->loadByIdPath($id_path);
		 
		if (!$mainUrlRewrite->isObjectNew()) {
			$urlRewriteCollection = Mage::getModel('core/url_rewrite')->getCollection()
				->addFilter('target_path', $mainUrlRewrite->getRequestPath())
				->addFieldToFilter('url_rewrite_id', array('neq' => $mainUrlRewrite->getUrlRewriteId()))
				->load();
		 
			foreach ($urlRewriteCollection as $urlRewrite) {
		 	
				if ($urlRewrite->getRequestPath() == $request_path) {
					$urlRewrite->delete();
				}else{
					$urlRewrite->setTargetPath($request_path)
						->setIsSystem(true)
						->setOptions('RP')
						->save();
				}
			}
		}
		
		$mainUrlRewrite->setIdPath($id_path)
			->setRequestPath($request_path)
			->setTargetPath($target_path)
			->setIsSystem(true);
		
		return $mainUrlRewrite->save();
			
	}
	
	protected function deleteUrlRewrites()
	{
		$model_id = $this->_getModelId();
		$id_path = self::MODEL_NAME.'/'.$model_id;
		
		$mainUrlRewrite = Mage::getModel('core/url_rewrite')
			->loadByIdPath($id_path);
		
		if (!$mainUrlRewrite->isObjectNew()) {
			$urlRewriteCollection = Mage::getModel('core/url_rewrite')->getCollection()
				->addFilter('target_path', $mainUrlRewrite->getRequestPath())
				->addFieldToFilter('url_rewrite_id', array('neq' => $mainUrlRewrite->getUrlRewriteId()))
				->load();
			
			foreach ($urlRewriteCollection as $urlRewrite) {
				$urlRewrite->delete();
			}
		
		}
		
		$mainUrlRewrite->delete();
	}
	
	public function getUrl()
	{
		$target = self::MODULE_NAME.'/'.self::MODEL_NAME.'/result/';
		$ancho = $this->getAncho();
		$perfil = $this->getPerfil();
		$rin = $this->getRin();
		
		return Mage::getUrl($target, array( 'wf-anchos' => $ancho, 'wf-perfiles' => $perfil, 'wf-rins' => $rin ) );
	}
	
	public function reCreateUrlRewrite(){
		
		$model_id = $this->_getModelId();
		$id_path = self::MODEL_NAME.'/'.$model_id;
	
		if( $this->handleUrlRewrite()->getUrlRewriteId() ){
		
			$mainUrlRewrite = Mage::getModel('core/url_rewrite')
				->loadByIdPath($id_path);
			
			if ($mainUrlRewrite->getId()) {
				
				return Mage::getUrl($mainUrlRewrite->getRequestPath());
				
			} else {
				return false;
			}
			
		}else{
			return false;
		}
		
	}
	
	public function getUrlRewrite()
	{
		$model_id = $this->_getModelId();
		$id_path = self::MODEL_NAME.'/'.$model_id;
		
		$mainUrlRewrite = Mage::getModel('core/url_rewrite')
			->loadByIdPath($id_path);
		
		if ($mainUrlRewrite->getId()) {
			return Mage::getUrl($mainUrlRewrite->getRequestPath());
		} else {
			return $this->reCreateUrlRewrite();
		}
	}
	
	public function checkIdentifier($identifier)
    {
        return $this->_getResource()->checkIdentifier($identifier);
    }
	
	public function unsetVehiclesSizes($id)
	{
		return $this->_getResource()->unsetVehiclesSizes($id);
	}
	
	public function getSizesOptionsArray()
	{
		$array_data = array();
		$collection = $this->getCollection();
		
		//$collection->setOrder('width', 'asc');
		
		$collection->getSelect()->order( array('ancho ASC', 'perfil ASC', 'rin ASC') );
		
		if(count($collection) > 0){
			foreach($collection as $size){
				$array_data[ $size->getId() ] = $size->getAncho().'/'.$size->getPerfil().' '.$size->getRin()."''";
			}
		}
		
		return $array_data;
	}
	
	public function getPerfilesByAncho($ancho)
	{

		$collection = $this->getCollection()
			->addFilter('ancho', $ancho)
			->setOrder('perfil', 'asc');
			
		$perfiles = array();
		
		foreach($collection as $size){
			
			if( !in_array($size->getPerfil(), $perfiles) ){
				$perfiles[] = $size->getPerfil();
			}
			
		}
		
		return $perfiles;
	}
	
	public function getRinsByAnchoAndPerfil($ancho, $perfil)
	{
		$collection = $this->getCollection()
			->addFilter('ancho', $ancho)
			->addFilter('perfil', $perfil)
			->setOrder('rin', 'asc');
			
		$rins = array();
		
		foreach($collection as $size){
		
			if( !in_array($size->getRin(), $rins) ){
				$rins[] = $size->getRin();
			}
			
		}
		
		return $rins;
	}
	
	public function getSizeIdByFields($ancho, $perfil, $rin)
	{
		return $this->_getResource()->getSizeIdByFields($ancho, $perfil, $rin);
	}

}