<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials data helper
 *
 * @category   Nicolas
 * @package    Nicolas_Testimonials
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Wheelsfinder_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function formatSavedSizes($saved)
	{
		$sizes_saved = '';
		$break = '';
		$i = 0;
		foreach($saved as $saved){
			if($i != 0){
				$break = ', ';
			}
			$size = Mage::getModel('wheelsfinder/sizes')->load($saved);
			
			$sizes_saved = $sizes_saved.$break.$size->getAncho().'/'.$size->getPerfil().' '.$size->getRin();
			$i++;
			//$saved_sizes = $saved_sizes.'<br />size_id: '.$saved['size_id'];
		}
		
		return $sizes_saved;
	}
	
	public function getVehicleNameById($ids)
	{
	
		$vehicles_name_string = '';
		$i = 0;
		$comma = '';
		$vehicle = Mage::getModel('wheelsfinder/vehicles');
		
		
		foreach($ids as $id){
			
			if($i != 0){
				$comma = ', ';
			}
			
			$vehicle->load($id);		
			$vehicles_name_string = $vehicles_name_string.$comma.$vehicle->getModelo().' - '.$vehicle->getMarca().' '.$vehicle->getLinea();

			$i++;

		}
		
		return $vehicles_name_string;
	}
	
	public function getDebugMode()
	{
		return Mage::getStoreConfig('wheelsfinder/configuration/debug');
	}
	
	public function vehiclesFormAvailable()
	{
		if(count(Mage::getModel('wheelsfinder/vehicles')->getCollection())){
			return true;
		}else{
			return false;
		}
	}
	
	public function sizesFormAvailable()
	{
		if(count(Mage::getModel('wheelsfinder/sizes')->getCollection())){
			return true;
		}else{
			return false;
		}
	}
	
}