<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Wheelsfinder_Block_Adminhtml_Vehicles_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'wheelsfinder';
        $this->_controller = 'adminhtml_vehicles';
        
        $this->_updateButton('save', 'label', Mage::helper('wheelsfinder')->__('Save Vehicle'));
        $this->_updateButton('delete', 'label', Mage::helper('wheelsfinder')->__('Delete Vehicle'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('wheelsfinder_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'wheelsfinder_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'wheelsfinder_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('vehicles_data') && Mage::registry('vehicles_data')->getId() ) {
            return Mage::helper('wheelsfinder')->__("Edit Vehicle '%s'", $this->htmlEscape( Mage::registry('vehicles_data')->getMarca().' '.Mage::registry('vehicles_data')->getModelo().' '.Mage::registry('vehicles_data')->getLinea() ));
        } else {
            return Mage::helper('wheelsfinder')->__('Add Vehicle');
        }
    }
}