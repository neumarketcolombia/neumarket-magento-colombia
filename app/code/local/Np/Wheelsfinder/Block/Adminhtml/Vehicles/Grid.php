<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Wheelsfinder_Block_Adminhtml_Vehicles_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('vehiclesGrid');
      $this->setDefaultSort('vehicles_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('wheelsfinder/vehicles')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('vehicles_id', array(
          'header'    => Mage::helper('wheelsfinder')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'vehicles_id',
      ));
	  
	   $this->addColumn('marca', array(
          'header'    => Mage::helper('wheelsfinder')->__('Marca'),
          'align'     =>'left',
          'index'     => 'marca',
      ));

      $this->addColumn('modelo', array(
          'header'    => Mage::helper('wheelsfinder')->__('Modelo'),
          'align'     =>'left',
          'index'     => 'modelo',
      ));
	  
	  $this->addColumn('linea', array(
          'header'    => Mage::helper('wheelsfinder')->__('Linea'),
          'align'     =>'left',
          'index'     => 'linea',
      ));

      $yesnoOptions = array('No' => 'No', 'Yes' => 'Si');
      $this->addColumn('img', array(
          'header'    => Mage::helper('wheelsfinder')->__('Imagen'),
          'align'     =>'center',
          'width'     => '70px',
          'index'     => 'hasimg',
          'type'      => 'options',
          'options'   => $yesnoOptions,
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('wheelsfinder')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('wheelsfinder')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		//$this->addExportType('*/*/exportCsv', Mage::helper('wheelsfinder')->__('CSV'));
		//$this->addExportType('*/*/exportXml', Mage::helper('wheelsfinder')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('vehicles_id');
        $this->getMassactionBlock()->setFormFieldName('vehicles');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('wheelsfinder')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('wheelsfinder')->__('Are you sure?')
        ));
		
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}