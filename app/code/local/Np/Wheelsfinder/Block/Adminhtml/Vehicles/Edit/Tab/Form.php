<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Wheelsfinder_Block_Adminhtml_Vehicles_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('vehicles_form', array('legend'=>Mage::helper('wheelsfinder')->__('Vehicle information')));
	  
	  $fieldset->addField('marca', 'text', array(
          'label'     => Mage::helper('wheelsfinder')->__('Marca'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'marca',
      ));
	  
      $fieldset->addField('modelo', 'text', array(
          'label'     => Mage::helper('wheelsfinder')->__('Modelo'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'modelo',
      ));
	  
	  
	  
	  $fieldset->addField('linea', 'text', array(
          'label'     => Mage::helper('wheelsfinder')->__('Linea'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'linea',
      ));

      $fieldset->addField('img', 'image', array(
          'label'     => Mage::helper('wheelsfinder')->__('Imagen'),
          'required'  => false,
          'note' => Mage::helper('catalog')->__('(Agregar msg sobre tamaño y formato de la imagen)'),
          'name'      => 'img',
      ));

      $fieldset->addField('mensaje', 'textarea', array(
           'label'     => Mage::helper('wheelsfinder')->__('Mensaje'),
           'required'  => false,
           'name'      => 'mensaje',
       ));

       /*$fieldset->addField('trim', 'text', array(
           'label'     => Mage::helper('wheelsfinder')->__('Trim'),
           'name'      => 'trim',
       ));*/
	  
	  
      if ( Mage::getSingleton('adminhtml/session')->getVehiclesData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getVehiclesData());
          Mage::getSingleton('adminhtml/session')->setVehiclesData(null);
      } elseif ( Mage::registry('vehicles_data') ) {
          $form->setValues(Mage::registry('vehicles_data')->getData());
      }
      return parent::_prepareForm();
  }
}