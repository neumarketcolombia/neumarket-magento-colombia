<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Wheelsfinder_Block_Adminhtml_Vehicles_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('vehicles_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('wheelsfinder')->__('Vehicle Information'));
  }
  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('wheelsfinder')->__('Vehicle Information'),
          'title'     => Mage::helper('wheelsfinder')->__('Vehicle Information'),
          'content'   => $this->getLayout()->createBlock('wheelsfinder/adminhtml_vehicles_edit_tab_form')->toHtml(),
      ));
	  
	  $this->addTab('form_section_sizes', array(
          'label'     => Mage::helper('wheelsfinder')->__('Vehicle Sizes'),
          'title'     => Mage::helper('wheelsfinder')->__('Vehicle Sizes'),
          'content'   => $this->getLayout()->createBlock('wheelsfinder/adminhtml_vehicles_edit_tab_sizes')->setTemplate('wheelsfinder/vehicles/edit/tabs/sizes/content.phtml')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}