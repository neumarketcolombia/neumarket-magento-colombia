<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Np_Wheelsfinder_Block_Finder_Size_List extends Mage_Catalog_Block_Product_List
{
	
	protected function _getSizesSession()
	{
		return Mage::getSingleton('core/session');
	}
	
	protected function _getProductCollection()
    {
		
		//$session = $this->_getSizesSession();
		//$size_id = $session->getData('id_size_result');
		
		//$request_params = $this->getRequest()->getParams();
		//unset($request_params['wf-anchos']);
		//unset($request_params['wf-perfiles']);
		//unset($request_params['wf-rins']);
		
		//$layer = $this->getLayer();
		//$layer->apply();
		//$this->_productCollection = $layer->getProductCollection();
		
		//echo count($this->_productCollection);
		//exit();
		
		/*if($size_id){
			
			$product_collection = Mage::getModel('catalog/product')
				->getCollection()
				->addAttributeToSelect('*')
				->addFieldToFilter('wheelsfinder_sizes', $size_id);
			
			//if(count($request_params)){
				//foreach($request_params as $attribute => $value){
					
					//$product_collection->addFieldToFilter($attribute, $value);
					
				//}
			//}
			
			$this->_productCollection = $product_collection;
			
		}else{
		
			if (is_null($this->_productCollection)) {
				
				$layer = $this->getLayer();
				$this->_productCollection = $layer->getProductCollection();
				
			}
		
		}*/
		
        //return $this->_productCollection;
		return parent::_getProductCollection();
		
    }
	
	public function getSearchName()
	{
		return 'Dimensi&oacute;n';
	}
	
	public function getSearchCriteria()
	{
		$session = $this->_getSizesSession();
		return $session->getData('criteria');
	}

}