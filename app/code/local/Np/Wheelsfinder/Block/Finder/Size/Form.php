<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Np_Wheelsfinder_Block_Finder_Size_Form extends Mage_Core_Block_Template
{
	protected function _getSizesSession()
	{
		return Mage::getSingleton('core/session');
	}
	
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
	
	/**
     * Retrieve wheelsfinder vehicles model object
     *
     * @return Np_Wheelsfinder_Model_Vehicles
     */
    public function getModel()
    {
        return Mage::getSingleton('wheelsfinder/sizes');
    }
	
	public function getSearchPostUrl()
    {
        return $this->getUrl('wheelsfinder/sizes/result', array('_secure'=>true));
    }
	
	public function getRecoveryUrl()
	{
		return $this->getUrl('wheelsfinder/sizes/recovery', array('_secure'=>true));
	}
	
	public function getAvailableAnchos()
	{
		$collection = $this->getModel()
			->getCollection()
			->setOrder('ancho', 'asc')
			->getColumnValues('ancho');
		
		$availables = array();
		
		foreach($collection as $size){
			if( !in_array($size, $availables) ){
				$availables[] = $size;
			}
		}
		
		return $availables;
	}
	
	public function getCurrentAnchoAndPerfiles()
	{
		$session = $this->_getSizesSession();
		$ancho = NULL;
		$perfiles = NULL;
		
		if( $session->getData('wf_ancho') ){
			
			$ancho = $session->getData('wf_ancho');
			$perfiles = Mage::getModel('wheelsfinder/sizes')->getPerfilesByAncho($ancho);
			
		}
		
		
		$return = array('wf-ancho-selected'=>$ancho, 'wf-perfiles'=>$perfiles);
		
		return $return;
	}
	
	public function getCurrentPerfilAndRins()
	{
		$session = $this->_getSizesSession();
		$ancho = NULL;
		$perfil = NULL;
		$rins = NULL;
		
		if( $session->getData('wf_ancho') && $session->getData('wf_perfil') ){
			
			$ancho = $session->getData('wf_ancho');
			$perfil = $session->getData('wf_perfil');
			$rins = Mage::getModel('wheelsfinder/sizes')->getRinsByAnchoAndPerfil($ancho, $perfil);
			
		}
		
		$return = array('wf-perfil-selected'=>$perfil, 'wf-rins'=>$rins);
		
		
		
		return $return;
	}
	
	public function getCurrentRin()
	{
		$session = $this->_getSizesSession();
		$rin = NULL;
		
		if( $session->getData('wf_rin') ){
			
			$rin = $session->getData('wf_rin');
				
		}
		
		$return = array('wf-rin-selected'=>$rin);
		
		return $return;
	}

}