<?php
/**
 * Ezequiel Klusman
 */
$installer = $this;
$installer->startSetup();

$sql = "ALTER TABLE `{$installer->getTable('wheelsfinder_vehicles')}`
		ADD `hasimg` VARCHAR( 3 ) NOT NULL DEFAULT 'No' AFTER `img`;
		";
$installer->run($sql);

$installer->endSetup(); 