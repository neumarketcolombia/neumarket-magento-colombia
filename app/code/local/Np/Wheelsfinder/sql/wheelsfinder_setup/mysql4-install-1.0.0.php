<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
$installer = $this;
$installer->startSetup();

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('wheelsfinder_sizes')};
CREATE TABLE IF NOT EXISTS {$this->getTable('wheelsfinder_sizes')} (
  `sizes_id` int(11) unsigned NOT NULL auto_increment,
  `ancho` varchar(255) NOT NULL default '',
  `perfil` varchar(255) NOT NULL default '',
  `rin` varchar(255) NOT NULL default '',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`sizes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('wheelsfinder_vehicles')};
CREATE TABLE IF NOT EXISTS {$this->getTable('wheelsfinder_vehicles')} (
  `vehicles_id` int(11) unsigned NOT NULL auto_increment,
  `marca` varchar(255) NOT NULL default '',
  `modelo` varchar(255) NOT NULL default '',
  `linea` varchar(255) NOT NULL default '',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`vehicles_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('wheelsfinder_vehicles_sizes')};
CREATE TABLE IF NOT EXISTS {$this->getTable('wheelsfinder_vehicles_sizes')} (
  `vehicles_sizes_id` int(11) unsigned NOT NULL auto_increment,
  `size_id` varchar(255) NOT NULL default '',
  `vehicle_id` varchar(255) NOT NULL default '',
  PRIMARY KEY (`vehicles_sizes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->installEntities();

$installer->endSetup(); 