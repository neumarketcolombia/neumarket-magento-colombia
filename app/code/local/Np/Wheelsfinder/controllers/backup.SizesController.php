<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials controller
 *
 * @category   Nicolas
 * @package    Nicolas_Testimonials
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Wheelsfinder_sizesController extends Mage_Core_Controller_Front_Action
{
	
	protected function _getSizesSession()
	{
		return Mage::getSingleton('core/session');
	}
    
	public function indexAction()
    {
		$url = Mage::app()->getStore()->getBaseUrl();
		Mage::app()->getResponse()->setRedirect( $url );
    }
	
	public function recoveryAction()
    {
		$data = $this->getRequest()->getPost();

		$ancho = NULL;
		$perfil = NULL;
		$rin = NULL;
		
		if( isset($data['wf-anchos']) and !empty($data['wf-anchos']) ){
			$ancho = $data['wf-anchos'];
		}
		
		if( isset($data['wf-perfiles']) and !empty($data['wf-perfiles']) ){
			$perfil = $data['wf-perfiles'];
		}
		
		if($ancho && !$perfil && !$rin){
			
			$this->_getSizesSession()->setData('wf_ancho', $ancho);
			Mage::registry('wf_ancho', $ancho);
			
			$perfiles = Mage::getModel('wheelsfinder/sizes')->getPerfilesByAncho($ancho);
			
			$response = new Varien_Object();
			$response->setData('data_perfiles', $perfiles);
			
			return $this->getResponse()->setBody( Mage::helper('core')->jsonEncode($response) );
			
		}
		
		if(!$ancho && $perfil && !$rin){
			
			$this->_getSizesSession()->setData('wf_perfil', $perfil);
			Mage::registry('wf_perfil', $perfil);
			
			$rins = Mage::getModel('wheelsfinder/sizes')->getRinsByAnchoAndPerfil($this->_getSizesSession()->getData('wf_ancho'), $perfil);
				
			$response = new Varien_Object();
			$response->setData('data_rins',$rins);
			
			return $this->getResponse()->setBody( Mage::helper('core')->jsonEncode($response) );
			
		}
		
    }
	
	public function resultAction()
    {	 
		$aliases = $this->getRequest()->getAliases();
		$redirect_rewrite = NULL;
		
		if(!count($aliases) && !isset($aliases['rewrite_request_path']) ){
			$redirect_rewrite = true;
		}
		
		$data = $this->getRequest()->getParams();
		$session = $this->_getSizesSession();
		
		$ancho = NULL;
		$perfil = NULL;
		$rin = NULL;
		$title = NULL;
		$criteria = array();
		
		if( isset($data['wf-anchos']) and !empty($data['wf-anchos']) ){
			$ancho = $data['wf-anchos'];
			$session->setData('wf_ancho', $ancho);
			
			Mage::registry('wf_ancho', $ancho);
			
			$title = $title.$ancho;
			$criteria['Ancho'] = $ancho;
		}
		
		if( isset($data['wf-perfiles']) and !empty($data['wf-perfiles']) ){
			$perfil = $data['wf-perfiles'];
			$session->setData('wf_perfile', $perfil);
			
			Mage::registry('wf_perfile', $perfil);
			
			$title = $title.' '.$perfil;
			$criteria['Perfil'] = $perfil;
		}
		
		if( isset($data['wf-rins']) and !empty($data['wf-rins']) ){
			$rin = $data['wf-rins'];
			$session->setData('wf_rin', $rin);
			
			Mage::registry('wf_rin', $rin);
			
			$title = $title.' R'.$rin;
			$criteria['Rin'] = $rin;
		}
		
		//var_dump($perfil);
		//exit();
		
		$size_id = Mage::getModel('wheelsfinder/sizes')->getSizeIdByFields($ancho, $perfil, $rin);
		
		if($redirect_rewrite){
			$size = Mage::getModel('wheelsfinder/sizes')->load($size_id);
			if($size->getUrlRewrite()){
				Mage::app()->getResponse()->setRedirect($size->getUrlRewrite());
			}
		}
		
		$session
			->setData('size_results', $size_id)
			->setData('criteria', $criteria);
			//->addSuccess($this->__('Resultados de busqueda por medidas: ').$title."''");
		
		$this->loadLayout()
			->getLayout()->getBlock('head')
			->setDescription($this->__('Compra tus Llantas ').$title.(' en Neumarket Colombia. Llama al 7435595 en Bogota - 018000 185595 Resto del pais.'))
			->setKeywords($this->__('llantas ').$title.(' neumarket colombia bogota'))
			->setTitle($this->__('Llantas ').$title.(' | Neumarket'));
	
		$this->renderLayout();
		
    }
}