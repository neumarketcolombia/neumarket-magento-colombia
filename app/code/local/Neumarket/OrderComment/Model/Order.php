<?php

require_once 'Mage/Sales/Model/Order.php';
class Neumarket_OrderComment_Model_Order extends Mage_Sales_Model_Order
{

    public function addStatusHistoryComment($comment, $status = false)
    {
        if(Mage::getSingleton('admin/session')->isLoggedIn())
        {
            //getting username
            $user = Mage::getSingleton('admin/session');
            $firstname = $user->getUser()->getFirstname();
            $lastname = $user->getUser()->getLastname();
            //$username = $user->getUser()->getUsername();
            $append = " (".$firstname." ".$lastname.")";
        }
        else
        {
            $append = "";
        }
        if (false === $status) {
            $status = $this->getStatus();
        } elseif (true === $status) {
            $status = $this->getConfig()->getStateDefaultStatus($this->getState());
        } else {
            $this->setStatus($status);
        }
        $history = Mage::getModel('sales/order_status_history')
            ->setStatus($status)
            ->setComment($comment.$append)
            ->setEntityName($this->_historyEntityName);
        $this->addStatusHistory($history);
        return $history;
    }

}
