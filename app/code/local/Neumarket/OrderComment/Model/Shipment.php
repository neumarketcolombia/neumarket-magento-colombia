<?php

require_once 'Mage/Sales/Model/Order/Shipment.php';
class Neumarket_OrderComment_Model_Shipment extends Mage_Sales_Model_Order_Shipment
{

    public function addComment($comment, $notify=false, $visibleOnFront=false)
    {
        if (!($comment instanceof Mage_Sales_Model_Order_Shipment_Comment)) {
            //getting username
            $user = Mage::getSingleton('admin/session');
            $firstname = $user->getUser()->getFirstname();
            $lastname = $user->getUser()->getLastname();
            //$username = $user->getUser()->getUsername();
            $comment = Mage::getModel('sales/order_shipment_comment')
                ->setComment($comment." (".$firstname." ".$lastname.")")
                ->setIsCustomerNotified($notify)
                ->setIsVisibleOnFront($visibleOnFront);
        }
        $comment->setShipment($this)
            ->setParentId($this->getId())
            ->setStoreId($this->getStoreId());
        if (!$comment->getId()) {
            $this->getCommentsCollection()->addItem($comment);
        }
        $this->_hasDataChanges = true;
        return $this;
    }

}