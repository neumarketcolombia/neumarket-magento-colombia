<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 7/15/17
 * Time: 7:01 PM
 */



class Herfox_Payucash_Model_Payucash extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'payucash';
    protected $_formBlockType = 'payucash/form_payucash';
    protected $_infoBlockType = 'payucash/info_payucash';
    protected $_isGateway = true;

    public function assignData($data)
    {
        $info = $this->getInfoInstance();

        $info->setCashMethod($data->getCashMethod());

        return $this;
    }

    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('payucash/payment/redirect', array('_secure' => false));
    }
}