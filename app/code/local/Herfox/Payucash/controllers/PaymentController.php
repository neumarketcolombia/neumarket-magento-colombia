<?php

/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 7/20/17
 * Time: 8:37 PM
 */
class Herfox_Payucash_PaymentController extends Mage_Core_Controller_Front_Action
{
    public function redirectAction()
    {
        $response = Mage::helper('payucash')->payUAuthorizeAndCapture();

        if ($response->code == "SUCCESS") {
            $transaction = $response->transactionResponse;
            if ($transaction->state == 'APPROVED' || $response->transactionResponse->state == 'PENDING') {
                $this->_redirectUrl($transaction->extraParameters->URL_PAYMENT_RECEIPT_HTML);
            } else {
                Mage::getSingleton('checkout/session')->addError("El comprabante no ha sido generado porque " . Mage::helper('payupse')->getErrorMessage($transaction->responseCode) . " Por favor intente nuevamente o pruebe con otro método de pago.");
                $this->_redirect('onestepcheckout/');
            }
        } else {
            Mage::getSingleton('checkout/session')->addError("Problemas con la pasarela de pagos. Por favor contacte con el administrador del sistema.");
            $this->_redirect('checkout/cart');
        }
    }

    public function responseAction()
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);

        Mage::getSingleton('checkout/session')->addWarning("Estaremos atentos a su pago. Si tiene alguna inquietud puede comunicarse con nosotros en Bogotá al 743 5595, en el resto del país a nuestra linea gratuita 01 8000 185 595 o escribirnos a nuestro Whatsapp 315 6075041");
        $order->setState(Mage_Sales_Model_Order::STATE_NEW, true, 'Pendiente de verificar pago.');

        $order->save();

        Mage::getSingleton('checkout/session')->unsQuoteId();
        Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure' => false));
    }
}