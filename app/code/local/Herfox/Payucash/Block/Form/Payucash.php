<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 7/15/17
 * Time: 7:24 PM
 */

class Herfox_Payucash_Block_Form_Payucash extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('payucash/form/payucash.phtml');
    }
}
