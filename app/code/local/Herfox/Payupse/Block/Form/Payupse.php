<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 7/15/17
 * Time: 7:24 PM
 */

class Herfox_Payupse_Block_Form_Payupse extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('payupse/form/payupse.phtml');
    }
}
