<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 7/15/17
 * Time: 7:13 PM
 */

require_once(Mage::getBaseDir('lib') . DS . 'PayU' . DS . 'PayU.php');

class Herfox_Payupse_Helper_Data extends Mage_Core_Helper_Abstract
{
    private function payUConnect()
    {
        if (!class_exists('PayU')) {
            Mage::log("Error: Problemas con la clase de PayU", null, 'opayupse.log');
            error_log("Problemas con la libreria de PayU", 0);
            throw new Mage_Payment_Model_Info_Exception("Problemas con este metodo de pago. Por favor informe al administrator del sistema.");
        }

        PayU::$apiKey = Mage::getStoreConfig('payment/payucard/api_key');
        PayU::$apiLogin = Mage::getStoreConfig('payment/payucard/api_login'); //Ingrese aquí su propio apiLogin.
        PayU::$merchantId = Mage::getStoreConfig('payment/payucard/merchant_id'); //Ingrese aquí su Id de Comercio.
        PayU::$language = SupportedLanguages::ES;

        if(Mage::getStoreConfig('payment/payucard/test')) {
            PayU::$isTest = true; //Dejarlo True cuando sean pruebas.
            Environment::setPaymentsCustomUrl("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi");
        }
        else {
            PayU::$isTest = false;
            Environment::setPaymentsCustomUrl("https://api.payulatam.com/payments-api/4.0/service.cgi");
        }
    }

    public function getBanks()
    {
        $this->payUConnect();

        $parameters = array(
            PayUParameters::PAYMENT_METHOD => "PSE", //Ingrese aquí el identificador de la cuenta.
            PayUParameters::COUNTRY => PayUCountries::CO //Ingrese aquí el nombre del pais.
        );

        $array=PayUPayments::getPSEBanks($parameters);
        $banks=$array->banks;

        return $banks;
    }

    public function payUAuthorizeAndCapture()
    {
        $this->payUConnect();
        $parameters = $this->pseSetParameters();
        if($parameters) {
            return PayUPayments::doAuthorizationAndCapture($parameters);
        }
        return false;
    }

    private function updateCustomer($dni)
    {
        $customer = Mage::getModel('customer/session')->getCustomer();

        // Si cliente NO tiene definidos los campos personalizados (Amasty)
        // y si se esta intentando registrar desde el checkout por primera vez:

        // Si el cliente es Persona
        if($dni['client_type'] == '1386' && !empty($dni['nro_documento']))
        {
            $customer->setData("nro_documento", $dni['nro_documento']);
            $customer->setData("tipo_documento", $dni['tipo_documento']);
            $customer->setData("client_type", $dni['client_type']);
            $customer->save();
        }
        // Si el cliente es Empresa
        else if($dni['client_type'] == '1387' && !empty($dni['nit']))
        {
            $customer->setData("nit", $dni['nit']);
            $customer->setData("razon_social", $dni['razon_social']);
            $customer->setData("client_type", $dni['client_type']);
            $customer->save();
        }

        return $customer;
    }

    private function pseSetParameters()
    {
        $checkout = Mage::getSingleton('checkout/session');
        $order = Mage::getModel('sales/order')->loadByIncrementId($checkout->getLastRealOrderId());
        $quote = Mage::getModel('sales/quote')->load($checkout->getLastQuoteId());
        $dni = $checkout->getAmcustomerattr();
        $payment = $quote->getPayment();
        $billing = $order->getBillingAddress();
        $email = $quote->getBillingAddress()->getEmail();

        // Si cliente esta logueado
        if (Mage::getModel('customer/session')->isLoggedIn())
        {
            // Actualizar cliente con datos de Amasty (Custom attributes)
            $customer = $this->updateCustomer($dni);

            $tipo_documento = $customer['tipo_documento'];
            $client_type = $customer['client_type'];
            if($client_type == "1386"){
                $document_number = $customer['nro_documento'];
            }
            else {
                $document_number = $customer['nit'];
            }
        }
        // Si el cliente es invitado
        else
        {
            $tipo_documento = $dni['tipo_documento'];
            $client_type = $dni['client_type'];
            if($client_type == "1386"){
                $document_number = $dni['nro_documento'];
            }
            else {
                $document_number = $dni['nit'];
            }
        }

        // Conversiones para atributos de PayU
        if($client_type == "1386")
        {
            $person_type = 'N';
            switch ($tipo_documento){
                case "1391":
                    $document_type = "CC";
                    break;
                case "1392":
                    $document_type = "CE";
                    break;
                case "1393":
                    $document_type = "PP";
                    break;
                default:
                    $document_type = "";
                    break;
            }
        }
        else
        {
            $person_type = 'J';
            $document_type = 'NIT';
        }

        $parameters = array(
            PayUParameters::ACCOUNT_ID => Mage::getStoreConfig('payment/payucard/account_id'), //Ingrese aquí el identificador de la cuenta.
            PayUParameters::REFERENCE_CODE => $order->getIncrementId() . "-" . rand(1000, 9999), //Ingrese aquí el código de referencia.
            PayUParameters::DESCRIPTION => "Compra en Neumarlet con Tarjeta Débito", //Ingrese aquí la descripción.
            PayUParameters::VALUE => number_format($order->getGrandTotal(), 2, '.', ''), //Ingrese aquí el valor.
            PayUParameters::CURRENCY => "COP", //Ingrese aquí la moneda.

            PayUParameters::BUYER_EMAIL => $email,//Ingrese aquí el email del comprador.
            PayUParameters::PAYER_NAME => preg_replace('!\s+!', ' ', $billing->getFirstname() . ' ' . $billing->getLastname()),//Ingrese aquí el nombre del pagador.
            PayUParameters::PAYER_EMAIL => $email,//Ingrese aquí el email del pagador.
            PayUParameters::PAYER_CONTACT_PHONE => $billing->getTelephone(),//Ingrese aquí el teléfono de contacto del pagador.

            // -- infarmación obligatoria para PSE --
            PayUParameters::PSE_FINANCIAL_INSTITUTION_CODE => $payment->getPseBank(),//Ingrese aquí el código pse del banco.
            PayUParameters::PAYER_PERSON_TYPE => $person_type,//Ingrese aquí el tipo de persona (N natural o J jurídica)
            PayUParameters::PAYER_DNI => $document_number,//Ingrese aquí el documento de contacto del pagador.
            PayUParameters::PAYER_DOCUMENT_TYPE => $document_type,//Ingrese aquí el tipo de documento del pagador: CC, CE, NIT, TI, PP,IDC, CEL, RC, DE.
            PayUParameters::PAYMENT_METHOD => "PSE",//Ingrese aquí el nombre del método de pago
            PayUParameters::COUNTRY => PayUCountries::CO,//Ingrese aquí el nombre del pais.

            PayUParameters::IP_ADDRESS => Mage::helper('core/http')->getRemoteAddr(),//IP del pagadador
            PayUParameters::PAYER_COOKIE => Mage::getModel('core/cookie')->get('frontend'),//Cookie de la sesión actual.
            PayUParameters::USER_AGENT => Mage::helper('core/http')->getHttpUserAgent(),//Cookie de la sesión actual.

            PayUParameters::RESPONSE_URL => Mage::getBaseUrl() . "payupse/payment/response"//Página de respuesta a la cual será redirigido el pagador.
        );

        return $parameters;
    }

    public function getErrorMessage($responseCode)
    {
        $error = "";
        switch ($responseCode) {
            case "ERROR":
                $error = "Ocurrió un error general.";
                break;
            case "APPROVED":
                $error = "La transacción fue aprobada.";
                break;
            case "ANTIFRAUD_REJECTED":
                $error = "La transacción fue rechazada por el sistema anti-fraude.";
                break;
            case "PAYMENT_NETWORK_REJECTED":
                $error = "La red financiera rechazó la    transacción.";
                break;
            case "ENTITY_DECLINED":
                $error = "La transacción fue declinada por el banco o por la red financiera debido a un error.";
                break;
            case "INTERNAL_PAYMENT_PROVIDER_ERROR":
                $error = "Ocurrió un error en el sistema intentando procesar el pago.";
                break;
            case "INACTIVE_PAYMENT_PROVIDER":
                $error = "El proveedor de pagos no se encontraba activo.";
                break;
            case "DIGITAL_CERTIFICATE_NOT_FOUND":
                $error = "La red financiera reportó un error en la autenticación.";
                break;
            case "INVALID_EXPIRATION_DATE_OR_SECURITY_CODE":
                $error = "El código de seguridad o la fecha de expiración estaba inválido.";
                break;
            case "INVALID_RESPONSE_PARTIAL_APPROVAL":
                $error = "Tipo de respuesta no válida. La entidad aprobó parcialmente la transacción y debe ser cancelada automáticamente por el sistema.";
                break;
            case "INSUFFICIENT_FUNDS":
                $error = "La cuenta no tenía fondos suficientes.";
                break;
            case "CREDIT_CARD_NOT_AUTHORIZED_FOR_INTERNET_TRANSACTIONS":
                $error = "La tarjeta de crédito no estaba autorizada para transacciones por Internet.";
                break;
            case "INVALID_TRANSACTION":
                $error = "La red financiera reportó que la transacción fue inválida.";
                break;
            case "INVALID_CARD":
                $error = "La tarjeta es inválida.";
                break;
            case "EXPIRED_CARD":
                $error = "La tarjeta ya expiró.";
                break;
            case "RESTRICTED_CARD":
                $error = "La tarjeta presenta una restricción.";
                break;
            case "CONTACT_THE_ENTITY":
                $error = "Debe contactar al banco.";
                break;
            case "REPEAT_TRANSACTION":
                $error = "Se debe repetir la transacción.";
                break;
            case "ENTITY_MESSAGING_ERROR":
                $error = "La red financiera reportó un error de comunicaciones con el banco.";
                break;
            case "BANK_UNREACHABLE":
                $error = "El banco no se encontraba disponible.";
                break;
            case "EXCEEDED_AMOUNT":
                $error = "La transacción excede un monto establecido por el banco.";
                break;
            case "NOT_ACCEPTED_TRANSACTION":
                $error = "La transacción no fue aceptada por el banco por algún motivo.";
                break;
            case "ERROR_CONVERTING_TRANSACTION_AMOUNTS":
                $error = "Ocurrió un error convirtiendo los montos a la moneda de pago.";
                break;
            case "EXPIRED_TRANSACTION":
                $error = "La transacción expiró.";
                break;
            case "PENDING_TRANSACTION_REVIEW":
                $error = "La transacción fue detenida y debe ser revisada, esto puede ocurrir por filtros de seguridad.";
                break;
            case "PENDING_TRANSACTION_CONFIRMATION":
                $error = "La transacción está pendiente de ser confirmada.";
                break;
            case "PENDING_TRANSACTION_TRANSMISSION":
                $error = "La transacción está pendiente para ser trasmitida a la red financiera. Normalmente esto aplica para transacciones con medios de pago en efectivo.";
                break;
            case "PAYMENT_NETWORK_BAD_RESPONSE":
                $error = "El mensaje retornado por la red financiera es inconsistente.";
                break;
            case "PAYMENT_NETWORK_NO_CONNECTION":
                $error = "No se pudo realizar la conexión con la red financiera.";
                break;
            case "PAYMENT_NETWORK_NO_RESPONSE":
                $error = "La red financiera no respondió.";
                break;
            case "FIX_NOT_REQUIRED":
                $error = "Clínica de transacciones: Código de manejo interno.";
                break;
        };

        return $error;
        //throw new Mage_Payment_Model_Info_Exception("Su tarjeta ha sido declinada porque " . $error . " Por favor intente nuevamente o pruebe con otro método de pago.");
    }
}
