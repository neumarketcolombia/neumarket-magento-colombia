<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 7/21/17
 * Time: 2:34 PM
 */

$installer = $this;
$installer->startSetup();
$installer->run("
ALTER TABLE `{$installer->getTable('sales/quote_payment')}` ADD `pse_bank` VARCHAR( 255 ) NULL;
ALTER TABLE `{$installer->getTable('sales/order_payment')}` ADD `pse_bank` VARCHAR( 255 ) NULL;
");
$installer->endSetup();