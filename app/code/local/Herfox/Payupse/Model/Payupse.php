<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 7/15/17
 * Time: 7:01 PM
 */



class Herfox_Payupse_Model_Payupse extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'payupse';
    protected $_formBlockType = 'payupse/form_payupse';
    protected $_infoBlockType = 'payupse/info_payupse';
    protected $_isGateway = true;

    public function assignData($data)
    {
        $info = $this->getInfoInstance();

        $info->setPseBank($data->getPseBank());

        return $this;
    }

    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('payupse/payment/redirect', array('_secure' => false));
    }
}