<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 7/20/17
 * Time: 8:37 PM
 */

class Herfox_Payupse_PaymentController extends Mage_Core_Controller_Front_Action
{
    public function redirectAction()
    {
        $response = Mage::helper('payupse')->payUAuthorizeAndCapture("payupse");

        if ($response->code == "SUCCESS") {
            $transaction = $response->transactionResponse;
            if ($transaction->state == 'APPROVED' || $response->transactionResponse->state == 'PENDING') {
                $this->_redirectUrl($transaction->extraParameters->BANK_URL);
            }
            else {
                Mage::getSingleton('checkout/session')->addError("Su tarjeta ha sido declinada porque ".Mage::helper('payupse')->getErrorMessage($transaction->responseCode)." Por favor intente nuevamente o pruebe con otro método de pago.");
                $this->_redirect('checkout/cart/');
            }
        }
        else {
            Mage::getSingleton('checkout/session')->addError("Problemas con la pasarela de pagos. Por favor contacte con el administrador del sistema.");
            $this->_redirect('checkout/cart');
        }
    }

    public function responseAction()
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
        $response = $this->getRequest()->getParams();

        if($response['estado_pol'] == 4 || $response['estado_pol'] == 12) {

            if($response['codigo_respuesta_pol'] == 1) {
                $message = 'Pago exitoso.';
                Mage::getSingleton('checkout/session')->addSuccess("Su pago fue aprobado y vamos a procesar su pedido.");
                if ($order->canInvoice()) {
                    $invoice = $order->prepareInvoice();
                    $invoice->pay()->register();
                    Mage::getModel('core/resource_transaction')
                        ->addObject($invoice)
                        ->addObject($invoice->getOrder())
                        ->save();

                    $invoice->sendEmail(true, '');
                    $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true, $message)->save();
                }
            }
            else if($response['codigo_respuesta_pol'] == 9994) {
                $message = "Transacción pendiente, por favor revisar si el débito fue realizado en el banco.";
                Mage::getSingleton('checkout/session')->addWarning($message);
                $order->setState(Mage_Sales_Model_Order::STATE_NEW, true, $message)->save();
            }

            $order->save();

            Mage::getSingleton('checkout/session')->unsQuoteId();
            Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=> false));
        }
        else {
            $message = "Ha cancelado la transacción manualmente o hay problemas con el sistema de pagos PSE, por favor intente nuevamente.";
            Mage::getSingleton('checkout/session')->addError($message);
            $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true, $message)->save();
            //Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=> false));
            $this->_redirect('checkout/cart/');
        }
    }

}