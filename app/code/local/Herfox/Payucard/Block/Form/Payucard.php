<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 7/15/17
 * Time: 7:24 PM
 */

class Herfox_Payucard_Block_Form_Payucard extends Mage_Payment_Block_Form_Ccsave
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('payucard/form/payucard.phtml');
    }
}
