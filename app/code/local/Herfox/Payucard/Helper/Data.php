<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 7/15/17
 * Time: 7:13 PM
 */

require_once(Mage::getBaseDir('lib') . DS . 'PayU' . DS . 'PayU.php');

class Herfox_Payucard_Helper_Data extends Mage_Core_Helper_Abstract
{
    private function payUConnect()
    {
        if (!class_exists('PayU')) {
            Mage::log("Error: Problemas con la clase de PayU", null, 'opayucash.log');
            error_log("Problemas con la libreria de PayU", 0);
            throw new Mage_Payment_Model_Info_Exception("Problemas con este metodo de pago. Por favor informe al administrator del sistema.");
        }

        //$this->code = Mage::getModel('Herfox_Payupse_Model_Payupse')->getCode();

        PayU::$apiKey = Mage::getStoreConfig('payment/payucard/api_key');
        PayU::$apiLogin = Mage::getStoreConfig('payment/payucard/api_login'); //Ingrese aquí su propio apiLogin.
        PayU::$merchantId = Mage::getStoreConfig('payment/payucard/merchant_id'); //Ingrese aquí su Id de Comercio.
        PayU::$language = SupportedLanguages::ES;

        if(Mage::getStoreConfig('payment/payucard/test')) {
            PayU::$isTest = true; //Dejarlo True cuando sean pruebas.
            Environment::setPaymentsCustomUrl("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi");
        }
        else {
            PayU::$isTest = false;
            Environment::setPaymentsCustomUrl("https://api.payulatam.com/payments-api/4.0/service.cgi");
        }

    }

    public function payUAuthorizeAndCapture(Varien_Object $payment, $amount, $type)
    {
        $this->payUConnect();
        $parameters = $this->cardSetParameters($payment, $amount, $type);
        if($parameters) {
            return PayUPayments::doAuthorizationAndCapture($parameters);
        }
        return false;
    }

    private function cardSetParameters(Varien_Object $payment, $amount)
    {
        $checkout = Mage::getSingleton('checkout/session');
        $order = $payment->getOrder();
        $billing = $order->getBillingAddress()->getData();
        $quote = $payment->getOrder()->getQuote();
        $dni = $checkout->getAmcustomerattr();
        $email = $quote->getBillingAddress()->getEmail();

        $paymentMethod = $payment->getCcType();
        switch ($paymentMethod) {
            case "VI":
                $paymentMethod = "VISA";
                break;
            case "MC":
                $paymentMethod = "MASTERCARD";
                break;
            case "AE":
                $paymentMethod = "AMEX";
                break;
            case "OT":
                $paymentMethod = "CODENSA";
                break;
        }

        $tipo_documento = $dni['tipo_documento'];
        $client_type = $dni['client_type'];
        if ($client_type == "1386") {
            $document_number = $dni['nro_documento'];
        } else {
            $document_number = $dni['nit'];
        }

        // Conversiones para atributos de PayU
        if ($client_type == "1386") {
            $person_type = 'N';
            switch ($tipo_documento) {
                case "1391":
                    $document_type = "CC";
                    break;
                case "1392":
                    $document_type = "CE";
                    break;
                case "1393":
                    $document_type = "PP";
                    break;
                default:
                    $document_type = "";
                    break;
            }
        } else {
            $person_type = 'J';
            $document_type = 'NIT';
        }

        $parameters = array(
            PayUParameters::ACCOUNT_ID => Mage::getStoreConfig('payment/payucard/account_id'), //Ingrese aquí el identificador de la cuenta.
            PayUParameters::REFERENCE_CODE => $order->getIncrementId() . "-" . rand(1000, 9999), //Ingrese aquí el código de referencia.
            PayUParameters::DESCRIPTION => "Compra en Neumarket con Tarjeta de Crédito", //Ingrese aquí la descripción.
            PayUParameters::VALUE => $order->grandTotal, //Ingrese aquí el valor.
            //PayUParameters::VALUE => number_format($amount, 2, '.', ''),
            PayUParameters::CURRENCY => "COP", //Ingrese aquí la moneda.

            // -- Comprador
            PayUParameters::BUYER_NAME => preg_replace('!\s+!', ' ', $billing['firstname'] . ' ' . $billing['middlename'] . ' ' . $billing['lastname']), //Ingrese aquí el nombre del comprador.
            PayUParameters::BUYER_EMAIL => $email, //Ingrese aquí el email del comprador.
            PayUParameters::BUYER_CONTACT_PHONE => $billing['telephone'], //Ingrese aquí el teléfono de contacto del comprador.
            PayUParameters::BUYER_DNI => $document_number, //Ingrese aquí el documento de contacto del comprador.
            PayUParameters::BUYER_STREET => $billing['street'], //Ingrese aquí la dirección del comprador.
            PayUParameters::BUYER_STREET_2 => "",
            PayUParameters::BUYER_CITY => $billing['city'],
            PayUParameters::BUYER_STATE => $billing['region'],
            PayUParameters::BUYER_COUNTRY => $billing['country_id'],
            PayUParameters::BUYER_POSTAL_CODE => $billing['postcode'],
            PayUParameters::BUYER_PHONE => $billing['telephone'],

            // -- Pagador --
            PayUParameters::PAYER_NAME => $payment->getCcOwner(), //Ingrese aquí el nombre del pagador.
            //PayUParameters::PAYER_EMAIL => $email, //Ingrese aquí el email del pagador.
            //PayUParameters::PAYER_CONTACT_PHONE => $billing['telephone'], //Ingrese aquí el teléfono de contacto del pagador.
            PayUParameters::PAYER_DNI_TYPE => $document_type,
            //PayUParameters::PAYER_BUSINESS_NAME => $payment->getCcBusinessName(),
            PayUParameters::PAYER_DNI => $document_number, //Ingrese aquí el documento de contacto del pagador.
            //PayUParameters::PAYER_STREET => $billing['street'], //Ingrese aquí la dirección del pagador.
            //PayUParameters::PAYER_STREET_2 => "",
            //PayUParameters::PAYER_CITY => $billing['city'],
            //PayUParameters::PAYER_STATE => $billing['region'],
            //PayUParameters::PAYER_COUNTRY => $billing['country_id'],
            //PayUParameters::PAYER_POSTAL_CODE => $billing['postcode'],
            //PayUParameters::PAYER_PHONE => $billing['telephone'],

            // -- Datos de la tarjeta de crédito --
            PayUParameters::CREDIT_CARD_NUMBER => $payment->getCcNumber(), //Ingrese aquí el número de la tarjeta de crédito
            PayUParameters::CREDIT_CARD_EXPIRATION_DATE => $payment->getCcExpYear() . "/" . str_pad($payment->getCcExpMonth(), 2, '0', STR_PAD_LEFT), //Ingrese aquí la fecha de vencimiento de la tarjeta de crédito
            PayUParameters::CREDIT_CARD_SECURITY_CODE => $payment->getCcCid(), //Ingrese aquí el código de seguridad de la tarjeta de crédito
            PayUParameters::PAYMENT_METHOD => $paymentMethod, //Ingrese aquí el nombre de la tarjeta de crédito
            PayUParameters::INSTALLMENTS_NUMBER => $payment->getCcInstallments(), //Ingrese aquí el número de cuotas.
            PayUParameters::COUNTRY => PayUCountries::CO, //Ingrese aquí el nombre del pais.


            PayUParameters::DEVICE_SESSION_ID => $payment->getCcDsi(),//md5(Mage::getSingleton("core/session")->getSessionId().microtime()), //Session id del device.
            PayUParameters::IP_ADDRESS => Mage::helper('core/http')->getRemoteAddr(), //IP del pagadador
            PayUParameters::PAYER_COOKIE => Mage::getModel('core/cookie')->get('frontend'), //Cookie de la sesión actual.
            PayUParameters::USER_AGENT => Mage::helper('core/http')->getHttpUserAgent()//Cookie de la sesión actual.

        );

        return $parameters;


    }

    public function getErrorMessage($responseCode)
    {
        $error = "";
        switch ($responseCode) {
            case "ERROR":
                $error = "Ocurrió un error general.";
                break;
            case "APPROVED":
                $error = "La transacción fue aprobada.";
                break;
            case "ANTIFRAUD_REJECTED":
                $error = "La transacción fue rechazada por el sistema anti-fraude.";
                break;
            case "PAYMENT_NETWORK_REJECTED":
                $error = "La red financiera rechazó la    transacción.";
                break;
            case "ENTITY_DECLINED":
                $error = "La transacción fue declinada por el banco o por la red financiera debido a un error.";
                break;
            case "INTERNAL_PAYMENT_PROVIDER_ERROR":
                $error = "Ocurrió un error en el sistema intentando procesar el pago.";
                break;
            case "INACTIVE_PAYMENT_PROVIDER":
                $error = "El proveedor de pagos no se encontraba activo.";
                break;
            case "DIGITAL_CERTIFICATE_NOT_FOUND":
                $error = "La red financiera reportó un error en la autenticación.";
                break;
            case "INVALID_EXPIRATION_DATE_OR_SECURITY_CODE":
                $error = "El código de seguridad o la fecha de expiración estaba inválido.";
                break;
            case "INVALID_RESPONSE_PARTIAL_APPROVAL":
                $error = "Tipo de respuesta no válida. La entidad aprobó parcialmente la transacción y debe ser cancelada automáticamente por el sistema.";
                break;
            case "INSUFFICIENT_FUNDS":
                $error = "La cuenta no tenía fondos suficientes.";
                break;
            case "CREDIT_CARD_NOT_AUTHORIZED_FOR_INTERNET_TRANSACTIONS":
                $error = "La tarjeta de crédito no estaba autorizada para transacciones por Internet.";
                break;
            case "INVALID_TRANSACTION":
                $error = "La red financiera reportó que la transacción fue inválida.";
                break;
            case "INVALID_CARD":
                $error = "La tarjeta es inválida.";
                break;
            case "EXPIRED_CARD":
                $error = "La tarjeta ya expiró.";
                break;
            case "RESTRICTED_CARD":
                $error = "La tarjeta presenta una restricción.";
                break;
            case "CONTACT_THE_ENTITY":
                $error = "Debe contactar al banco.";
                break;
            case "REPEAT_TRANSACTION":
                $error = "Se debe repetir la transacción.";
                break;
            case "ENTITY_MESSAGING_ERROR":
                $error = "La red financiera reportó un error de comunicaciones con el banco.";
                break;
            case "BANK_UNREACHABLE":
                $error = "El banco no se encontraba disponible.";
                break;
            case "EXCEEDED_AMOUNT":
                $error = "La transacción excede un monto establecido por el banco.";
                break;
            case "NOT_ACCEPTED_TRANSACTION":
                $error = "La transacción no fue aceptada por el banco por algún motivo.";
                break;
            case "ERROR_CONVERTING_TRANSACTION_AMOUNTS":
                $error = "Ocurrió un error convirtiendo los montos a la moneda de pago.";
                break;
            case "EXPIRED_TRANSACTION":
                $error = "La transacción expiró.";
                break;
            case "PENDING_TRANSACTION_REVIEW":
                $error = "La transacción fue detenida y debe ser revisada, esto puede ocurrir por filtros de seguridad.";
                break;
            case "PENDING_TRANSACTION_CONFIRMATION":
                $error = "La transacción está pendiente de ser confirmada.";
                break;
            case "PENDING_TRANSACTION_TRANSMISSION":
                $error = "La transacción está pendiente para ser trasmitida a la red financiera. Normalmente esto aplica para transacciones con medios de pago en efectivo.";
                break;
            case "PAYMENT_NETWORK_BAD_RESPONSE":
                $error = "El mensaje retornado por la red financiera es inconsistente.";
                break;
            case "PAYMENT_NETWORK_NO_CONNECTION":
                $error = "No se pudo realizar la conexión con la red financiera.";
                break;
            case "PAYMENT_NETWORK_NO_RESPONSE":
                $error = "La red financiera no respondió.";
                break;
            case "FIX_NOT_REQUIRED":
                $error = "Clínica de transacciones: Código de manejo interno.";
                break;
        };

        return $error;
    }

}
