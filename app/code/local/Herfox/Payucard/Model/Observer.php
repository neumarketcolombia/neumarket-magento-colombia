<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 7/16/17
 * Time: 11:04 PM
 */

//require_once(Mage::getBaseDir('lib') . DS . 'PayU' . DS . 'PayU.php');

class Herfox_Payucard_Model_Observer
{
    public function updateCustomer()
    {
        // Si cliente esta logueado
        if (Mage::getModel('customer/session')->isLoggedIn())
        {
            $checkout = Mage::getSingleton('checkout/session');
            $dni = $checkout->getAmcustomerattr();

            $customer = Mage::getModel('customer/session')->getCustomer();

            // Si cliente NO tiene definidos los campos personalizados (Amasty)
            // y si se esta intentando registrar desde el checkout por primera vez:

            // Si el cliente es Persona
            if($dni['client_type'] == '1386' && !empty($dni['nro_documento']))
            {
                $customer->setData("nro_documento", $dni['nro_documento']);
                $customer->setData("tipo_documento", $dni['tipo_documento']);
                $customer->setData("client_type", $dni['client_type']);
                $customer->save();
            }
            // Si el cliente es Empresa
            else if($dni['client_type'] == '1387' && !empty($dni['nit']))
            {
                $customer->setData("nit", $dni['nit']);
                $customer->setData("razon_social", $dni['razon_social']);
                $customer->setData("client_type", $dni['client_type']);
                $customer->save();
            }
        }
    }
    public function processPayment($event)
    {
        $order = $event->getOrder();
        $payment = $order->getPayment();
        $state = $payment->getCcTransactionState();
        $message = Mage::helper('payucard')->getErrorMessage($payment->getCcResponseCode());
        $order->addStatusHistoryComment($message)->save();
        if($state == "PENDING") {
            Mage::getSingleton('checkout/session')->addWarning($message." Sin embargo hemos tomado su orden y estaremos pendiente de la confirmación de pago por parte de su entidad.");
        }
        else if($state == "APPROVED") {
            if ($order->canInvoice()) {
                $this->processOrderStatus($order);
            }
        }
    }

    private function processOrderStatus($order)
    {
        $invoice = $order->prepareInvoice();

        $invoice->register();
        Mage::getModel('core/resource_transaction')
            ->addObject($invoice)
            ->addObject($invoice->getOrder())
            ->save();

        $invoice->sendEmail(true, '');
        Mage::getSingleton('checkout/session')->addSuccess("Su pago fue aprobado y vamos a procesar su pedido.");
        $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
    }
}