<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 7/15/17
 * Time: 7:01 PM
 */

class Herfox_Payucard_Model_Payucard extends Mage_Payment_Model_Method_Cc
{
    protected $_code = 'payucard';
    protected $_formBlockType = 'payucard/form_payucard';
    protected $_infoBlockType = 'payucard/info_payucard';

    public function assignData($data)
    {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setCcType($data->getCcType())
            ->setCcOwner($data->getCcOwner())
            ->setCcLast4(substr($data->getCcNumber(), -4))
            ->setCcNumber($data->getCcNumber())
            ->setCcCid($data->getCcCid())
            ->setCcExpMonth($data->getCcExpMonth())
            ->setCcExpYear($data->getCcExpYear())
            ->setCcSsIssue($data->getCcSsIssue())
            ->setCcSsStartMonth($data->getCcSsStartMonth())
            ->setCcSsStartYear($data->getCcSsStartYear())
            ->setCcTypeDni($data->getCcTypeDni())
            ->setCcBusinessName($data->getCcBusinessName())
            ->setCcDni($data->getCcDni())
            ->setCcInstallments($data->getCcInstallments())
            ->setCcTransactionState($data->getCcTransactionState())
            ->setCcResponseCode($data->getCcResponseCode())
            ->setCcDsi($data->getCcDsi())
        ;
        return $this;
    }

    public function getVerificationRegEx()
    {
        return array_merge(parent::getVerificationRegEx(), array(
            'DINERS' => '/^[0-9]{3}$/'
        ));
    }

    public function OtherCcType($type)
    {
        return in_array($type, array(
            'OT', 'DINERS'
        ));
    }

    public function validate()
    {
        parent::validate();
        /* we call parent's validate() function!
         * if the code got this far, it means the cc type is none of the supported
         * ones implemented by Magento and now it gets interesting
         */

        $info = $this->getInfoInstance();
        $ccNumber = $info->getCcNumber();
        $availableTypes = explode(',',$this->getConfigData('cctypes'));
        // checks if Diners Club card is allowed
        if(!in_array($info->getCcType(), $availableTypes)){
            Mage::throwException($this->_getHelper()->__('Credit card type is not allowed for this payment method.'));
        }
        // validate credit card number against Luhn algorithm
        if(!$this->validateCcNum($info->getCcNumber())){
            Mage::throwException($this->_getHelper()->__('Invalid Credit Card Number'));
        }

        /* this is Diners Club regex pattern.
         * it's different for every cc type, so beware
         */
        if($info->getCcType()=='DINERS' && !preg_match('/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/', $ccNumber)){
            Mage::throwException($this->_getHelper()->__('Credit card number mismatch with credit card type.'));
        }

        // now we retrieve our CCV regex pattern and validate against it
        $verificationRegex = $this->getVerificationRegEx();
        if(!preg_match($verificationRegex[$info->getCcType()], $info->getCcCid()))
        {
            Mage::throwException($this->_getHelper()->__('Please enter a valid credit card verification number.'));
        }

        // further validation here (expiration month, expiration year etc.)

    }

    protected $_canAuthorize = true;

    public function authorize(Varien_Object $payment, $amount)
    {
        $errorMsg = "";
        $order = $payment->getOrder();
        $response = Mage::helper('payucard')->payUAuthorizeAndCapture($payment, $amount);

        if($response === false) {
            $errorCode = 'Invalid Data';
            $errorMsg = $this->_getHelper()->__('Error Processing the request');
        }
        else {
            if ($response->code == "SUCCESS") {
                $transaction = $response->transactionResponse;
                $payment->setTransactionId($transaction->transactionId);
                $payment->setTransactionAdditionalInfo(
                    Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS,
                    [
                        'orderId' => $transaction->orderId,
                        'state' => $transaction->state,
                        'responseCode' => $transaction->responseCode
                    ]
                );
                $payment->setCcTransactionState($transaction->state);
                $payment->setCcResponseCode($transaction->responseCode);
                if($transaction->state == "APPROVED") {
                    $payment->setIsTransactionClosed(1);
                }
                else {
                    $payment->setIsTransactionClosed(0);
                    if($transaction->state != "PENDING") {
                        $errorMsg = Mage::helper('payucard')->getErrorMessage($transaction->responseCode) . " Por favor intente nuevamente o pruebe con otro método de pago.";
                    }
                }
            }
            else {
                $errorMsg = "Problemas con la pasarela de pagos. Por favor contacte con el administrador del sistema.";
            }
        }
        if($errorMsg){
            Mage::throwException($errorMsg);
        }

        return $this;
    }
}