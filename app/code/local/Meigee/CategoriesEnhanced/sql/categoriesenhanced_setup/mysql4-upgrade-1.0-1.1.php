<?php
$installer = $this;
$installer->startSetup();
// Mega Menu
$installer->addAttribute('catalog_category', 'meigee_cat_icon', array(
    'group'             => 'General Information',
    'label'             => 'Font awesome menu icon',
    'note'              => "Font Awesome icon appears before category title. Full list of icons name you will find here: <a href='http://fortawesome.github.io/Font-Awesome/icons/'>Font awesome icon</a>. Icons will be displayed only for top categories and 1st level categories only.",
    'type'              => 'text',
    'input'             => 'text',
    'visible'           => true,
    'required'          => false,
    'backend'           => '',
    'frontend'          => '',
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'user_defined'      => true,
    'visible_on_front'  => true,
    'wysiwyg_enabled'   => true,
    'is_html_allowed_on_front'  => true,
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
	'sort_order' => 2,
));
$installer->endSetup();