{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="porter_content_slider"}}
<hr class="indent-18 white-space">
<div class="row">
	<div class="col-sm-4 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_wide_banner_1.jpg'}}" alt="">
		</a>
	</div>
	<hr class="indent-18 white-space visible-xs">
	<div class="col-sm-4 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_wide_banner_2.jpg'}}" alt="">
		</a>
	</div>
	<hr class="indent-18 white-space visible-xs">
	<div class="col-sm-4 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_wide_banner_3.jpg'}}" alt="">
		</a>
	</div>
</div>
<hr class="indent-40 white-space">
<header class="widget-title">
	<h3>Handpicked products</h3>
</header>
<div class="wide-product-slider">
	{{widget type="thememanager/widget_products" meigee_theme="porter" select_type="saleproducts" template="slider" porter_quickview="1" img_width="268" products_amount="5" product_name_slider="1" label_new="1" label_sale="1" price_slider="1" wishlist_slider="1" compare_slider="1" rating_stars_slider="1" rating_cust_link_slider="1" visible_products="4" items_horizontal="4" items_vertical="3" items_scaleup="false" pagination_speed="800" rewind_speed="1000" pagination="false" lazy_load="true" auto_play="5000" stop_on_hover="true" slide_speed="400" porter_center_loop="false" porter_rtl="false" widget_id="7945"}}
</div>
<hr class="indent-40 white-space">
<div class="row">
	<div class="col-sm-6 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_wide_banner_4.jpg'}}" alt="">
		</a>
	</div>
	<hr class="indent-18 white-space visible-xs">
	<div class="col-sm-6 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_wide_banner_5.jpg'}}" alt="">
		</a>
	</div>
</div>
<hr class="indent-40 white-space">
<header class="widget-title">
	<h3>Blazers</h3>
</header>
<div class="product-grid-wrapper">
	{{widget type="thememanager/widget_products" meigee_theme="porter" select_type="saleproducts" template="grid" porter_quickview="1" img_width="268" products_amount="6" products_per_row="6" product_name_grid="1" label_new="1" label_sale="1" price_grid="1" wishlist_grid="1" compare_grid="1" rating_stars_grid="1" rating_cust_link_grid="1" widget_id="1670"}}
</div>
<hr class="indent-40 white-space">
<header class="widget-title">
	<h3>Our brands</h3>
</header>
<hr class="indent-18 white-space">
<div class="row">
	<div class="col-sm-3 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_brand_1.png'}}" alt="">
		</a>
	</div>
	<hr class="indent-18 white-space visible-xs">
	<div class="col-sm-3 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_brand_2.png'}}" alt="">
		</a>
	</div>
	<hr class="indent-18 white-space visible-xs">
	<div class="col-sm-3 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_brand_3.png'}}" alt="">
		</a>
	</div>
	<hr class="indent-18 white-space visible-xs">
	<div class="col-sm-3 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_brand_4.png'}}" alt="">
		</a>
	</div>
</div>
<hr class="indent-40 white-space">
