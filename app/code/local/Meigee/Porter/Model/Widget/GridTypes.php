<?php
class Meigee_Porter_Model_Widget_GridTypes extends Meigee_Thememanager_Model_Widget_View_ThemeParametres
{
    const theme = 'Porter';



    public function toOptionArray()
    {
        $grid_types = $this->getConfig(self::theme, 'grid_types');
        return $grid_types['option'];
    }

}
