<?php
/**
 * User: Anh TO
 * Date: 3/24/14
 * Time: 3:07 PM
 */

class MW_FreeGift_Model_Observer_Layout extends Mage_Core_Model_Abstract{
    public function controllerLayoutBefore(Varien_Event_Observer  $observer){
        $action = $observer->getEvent()->getAction();
        $layout = $observer->getEvent()->getLayout();
        if(Mage::getDesign()->getArea() == 'frontend'){
            /** Create file config for javascript */
            $base_url = explode("/", Mage::getBaseUrl('js'));
            $versioninfo = Mage::getVersionInfo();
            $base_url = explode($base_url[count($base_url) - 2], Mage::getBaseUrl('js'));
            $js = "var mw_baseUrl = '{BASE_URL}';\n";

            $js = str_replace("{BASE_URL}", $base_url[0], $js);

            $js .= "var mw_ctrl = '".Mage::app()->getRequest()->getControllerName()."';\n";
            $js .= "var mw_mdn = '".Mage::app()->getRequest()->getModuleName()."';\n";
            $js .= "var mw_act = '".Mage::app()->getRequest()->getActionName()."';\n";
            $js .= "var version_num = '".$versioninfo['major'].".".$versioninfo['minor']."';\n";
            $js .= "var package_name = '".Mage::getSingleton('core/design_package')->getPackageName()."';\n";
            $js .= "var theme_name   = '".Mage::getSingleton('core/design_package')->getTheme('frontend')."';\n";

            $js .= 'var mw_session_id = "'.Mage::getSingleton("core/session")->getEncryptedSessionId().'";'."\n";

            $js .= "var isLogged = '".Mage::getSingleton('customer/session')->isLoggedIn()."';\n";
            if(Mage::helper('freegift/version')->isMageCommunity()){
                $version = 'mc';
            }else if(Mage::helper('freegift/version')->isMageEnterprise()){
                $version = 'me';
            }else if(Mage::helper('freegift/version')->isMageProfessional()){
                $version = 'mp';
            }

            $js .= "var version = '$version';\n";

            $js .= "window.hasGiftProduct = false;\n";
            $js .= "window.hasPromotionMessage = false;\n";
            $js .= "window.hasPromotionBanner = false;\n";
            file_put_contents(getcwd()."/js/mw_freegift/config.js", $js);
        }
    }
}