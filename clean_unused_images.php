<?php
$mageFilename = 'app/Mage.php';
require_once $mageFilename;


Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);

$media = Mage::getBaseDir('media') . '/catalog/product';
echo $media;
echo 'Query database for images <br/>';
$query = 'SELECT value FROM catalog_product_entity_media_gallery';
$data = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAll($query);

$dbData = array();
foreach ($data as $item) {
    $dbData[$item['value']] = $item['value'];
}
echo 'Images found in database:'.count($dbData) .'<br/>';

echo 'Search images in media directory <br/>';
$images = findFiles($media, array('jpg'));
echo 'Images found under directory($media):'.count($images['jpg']) .'<br/>';

echo 'Start removing images <br/>';
$removedCount = 0;
$skippedCount = 0;
$imgCount = 0;
foreach ($images['jpg'] as $image) {
    $imgCount++;
    if (strpos($image, 'cache') !== false) {
echo 'Skip cached image : '.$image.'<br/>';
        continue;
    }
	//echo $image;
    $imageCleanup = str_replace($media,'',$image);
    //echo $imageCleanup;
if (isset($dbData[$imageCleanup])) {
    echo 'Skip image is in database : '.$image.'<br/>';
$skippedCount++;
continue;
} else {
    echo 'Remove image : '.$image.'<br/>';
if ($testrun == false) unlink($image);
$removedCount++;
}
}

echo 'Done, removed '.$removedCount.' images and skipped '.$skippedCount.' images. count '.$imgCount.'. <br/>';

function findFiles($directory, $extensions = array())
{
    function glob_recursive($directory, &$directories = array())
    {
        foreach (glob($directory, GLOB_ONLYDIR | GLOB_NOSORT) as $folder) {
            $directories[] = $folder;
            glob_recursive($folder.'/*',$directories);
		}
	}
	glob_recursive($directory, $directories);
	$files = array ();
	foreach($directories as $directory) {
		foreach($extensions as $extension) {
			foreach(glob($directory.'/*.'.$extension) as $file) {
				$files[$extension][] = $file;
			}
		}
	}
	return $files;
}

?>