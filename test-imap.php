<?php
/**
 * Created by PhpStorm.
 * User: hernanforigua
 * Date: 3/01/18
 * Time: 2:56 PM
 */
date_default_timezone_set("America/Bogota");
ini_set('display_errors', 'ON');

require "app/Mage.php";
require "imap-attachment.php";
require "lib/PHPExcel.php";

Mage::init();

$email_addresses = [
    "coexito" => [
        "name" => "Coexito",
        "email"     => "ventasmayorbogota07@coexito.com.co",
        "sku_title" => "",
        "qty_title" => [],
        "prefix" => ""
    ],
    "redllantas" => [
        "name" => "Red Llantas",
        "email" => "isierra@redllantas.com",
        "sku_title" => "Código",
        "qty_title" => "Cantidad contable",
        "prefix" => "COL"
    ],
    "llantasytires" => [
        "name" => "Llantas y tires",
        "email" => "catalogo@neumarket.com",
        //"email" => "web@llantasytires.com",
        "sku_title" => "COD",
        "qty_title" => [
            "BOD PPAL",
            "BOD 3"
        ],
        "prefix" => "COL"
    ],
    "autofax" => [
        "name" => "Autofax",
        "email" => "",
        "sku_title" => "Código",
        "qty_title" => "Cantidad contable",
        "prefix" => "COL"
    ],
    "larueda" => [
        "name" => "La Rueda",
        "email" => "p.rosas@larueda.com.co",
        "sku_title" => "Código",
        "qty_title" => "Cantidad contable",
        "prefix" => "COL"
    ],
    "reencafe" => [
        "name" => "Reencafe",
        "email" => "ventas.pereira@reencafe.com",
        "sku_title" => "Código",
        "qty_title" => "Cantidad contable",
        "prefix" => "COL"
    ],
    "tecniruedas" => [
        "name" => "Tecniruedas",
        "email" => "aux.logistica@tecniruedas.com",
        "sku_title" => "Código",
        "qty_title" => "Cantidad contable",
        "prefix" => "TR"
    ],
    "colllantas" => [
        "name" => "Col Llantas",
        "email" => "estebanvelilla19@gmail.com",
        "sku_title" => "Código",
        "qty_title" => "Cantidad contable",
        "prefix" => "COL"
    ],
    "derco" => [
        "name" => "Derco",
        "email" => "carlosgamez@derco.com.co",
        "sku_title" => "Código",
        "qty_title" => "Cantidad contable",
        "prefix" => "COL"
    ]
];

$savedir = __DIR__ . '/imap-dump/';
$hostname = '{outlook.office365.com:993/imap/ssl}INBOX';
$username = 'hernan.forigua@neumarket.com';
$password = 'Foto8020';
$date = date("d M Y", strToTime("-21 days"));
$search = 'SUBJECT "Inventario Llantas y Tires" SINCE "' . $date . '"';

//$search = 'SUBJECT "Inventario" NEW SINCE "'.$date.'"';

$inbox = new IMAPMailbox($hostname, $username, $password);

$emails = $inbox->search($search, SE_FREE, "US-ASCII");

echo "<pre>";
var_dump($emails);
echo "</pre>";

if ($emails) {
    rsort($emails);
    foreach ($emails as $email) {

        $from_list = $email->headerInfo()->from;
        foreach ($from_list as $from) {
            $from_address = $from->mailbox . "@" . $from->host;
            $supplier = "";

            foreach ($email_addresses as $key => $value) {
                if (isset($value["email"])) {
                    if ($value["email"] == $from_address) {
                        foreach ($email->getAttachments() as $attachment) {
                            $savepath = $savedir . $attachment->getFilename();
                            if (file_put_contents($savepath, $attachment)) {
                                syncInventory($key, $value["name"], $savepath, $value["sku_title"], $value["qty_title"], $value["prefix"]);
                            }
                        }
                    }
                }
            }
        }
    }
}

function syncInventory($supplier, $name, $file, $sku_title, $qty_title, $prefix = "")
{
    $objPHPExcel = PHPExcel_IOFactory::load($file);
    $products = [];

    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
        $worksheetTitle = $worksheet->getTitle();
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

        $sku_col = false;
        $qty_col = false;

        // Check that the columns exist (For coexito don't apply)
        if ($supplier == "coexito" && ($worksheetTitle == "INV BOGOTA" || $worksheetTitle == "INV CALI")) {
            $sku_col = $qty_col = true;
        } else if ($sku_title && $qty_title){
            for ($col = 0; $col <= $highestColumnIndex; ++$col) {
                $cell = $worksheet->getCellByColumnAndRow($col, 1);
                $val = $cell->getValue();

                if ($val == $sku_title) {
                    $sku_col = true;
                    $sku_col_num = $col;
                }
                if ($val == $qty_title) {
                    $qty_col = true;
                    $qty_col_num = $col;
                }
            }
        }

        // If columns exist so load file
        if ($sku_col && $qty_col) {
            for ($row = 2; $row <= $highestRow; ++$row) {
                for ($col = 0; $col < $highestColumnIndex; ++$col) {
                    $cell = $worksheet->getCellByColumnAndRow($col, $row);

                    if ($supplier == "coexito") {
                        $col_val = trim(substr($cell->getValue(), 0, 23));
                        if (strpos($col_val, "HK") !== FALSE || strpos($col_val, "LF") !== FALSE) {
                            $sku_val = trim(substr($cell->getValue(), 0, 23));
                            $qty_val = (int)trim(substr($cell->getValue(), 67, 12));
                            //$name_val = trim(substr($cell->getValue(), 23, 44));
                            //$price_val = trim(substr($cell->getValue(), 79, 37));
                        }
                    } else {
                        if ($col == $sku_col_num) {
                            $sku_val = $cell->getValue();
                        } else if ($col == $qty_col_num) {
                            $qty_val = $cell->getValue();
                        }
                    }
                }
                if ($sku_val && $qty_val >= 0) {
                    $products[$prefix.$sku_val] += $qty_val;
                }
            }
        }
    }

    if(count($products)) {
        try {
            updateQty($name, $products);
            echo 'Proceso finalizado satisfactoriamente';
        }
        catch (Exception $e) {
            echo 'Error: ',  $e->getMessage(), "\n";
        }
    }
    else echo 'No se encontraron productos coincidientes.';
}

function updateQty($supplier, $data)
{
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
    foreach ($data as $sku => $qty) {
        $products = Mage::getModel('catalog/product')->getCollection();
        $products->addAttributeToFilter('nombre_proveedor', $supplier);
        $products->addAttributeToFilter('sku_proveedor', $sku);
        echo "<pre>";
        var_dump($products->getItems());
        echo "</pre>";
        foreach ($products as $product) {
            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
            $stockItem->setQty($qty);
            $stockItem->setIsInStock((int)($qty > 0));
            $stockItem->save();
        }
    }
}